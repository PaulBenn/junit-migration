package uk.co.paulbenn.jupiterify.config;

import lombok.Data;

import java.nio.file.Path;

@Data
public class Configuration {

    // -d, --dry-run
    private boolean dryRun = false;

    // -s, --source-sets
    private String[] sourceSets = { "test" };

    // -p, --project
    private Path projectRoot = null;

    // -o, --output
    private Path outputDirectory;

    // -X tabSize=<value>
    private int indent = 4;

    private boolean attemptWireMockMigration = false;

    private String wireMockExtension;

    // -X useCamelCase=<value>
    private boolean useCamelCase = false;
}
