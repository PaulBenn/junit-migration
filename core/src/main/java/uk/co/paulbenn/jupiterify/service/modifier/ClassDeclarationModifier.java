package uk.co.paulbenn.jupiterify.service.modifier;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ClassExpr;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;
import uk.co.paulbenn.jupiterify.service.CommentRegistry;
import uk.co.paulbenn.jupiterify.service.detector.Framework;
import uk.co.paulbenn.jupiterify.service.detector.FrameworkDetectors;
import uk.co.paulbenn.jupiterify.service.factory.ElementFactories;
import uk.co.paulbenn.jupiterify.unfixable.UnknownRunnerClass;
import uk.co.paulbenn.jupiterify.unfixable.WireMockSetUp;
import uk.co.paulbenn.jupiterify.util.Annotations;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ClassDeclarationModifier implements NodeModifier<ClassOrInterfaceDeclaration> {

    private final ElementFactories elementFactories;
    private final CommentRegistry commentRegistry;
    private final FrameworkDetectors frameworkDetectors;

    public ClassDeclarationModifier(ElementFactories elementFactories,
                                    CommentRegistry commentRegistry,
                                    FrameworkDetectors frameworkDetectors) {
        this.elementFactories = elementFactories;
        this.commentRegistry = commentRegistry;
        this.frameworkDetectors = frameworkDetectors;
    }

    @Override
    public boolean canModify(ClassOrInterfaceDeclaration node) {
        return true;
    }

    @Override
    public void modifyInPlace(ClassOrInterfaceDeclaration classDeclaration) {
        // Class should be package-private
        classDeclaration.setModifiers();

        // Remove "extends TestCase"
        removeExtendsTestCase(classDeclaration);

        // Determine new class annotations
        NodeList<AnnotationExpr> newAnnotations = computeClassAnnotations(classDeclaration);

        classDeclaration.getAnnotations().removeIf(x -> true);
        classDeclaration.setAnnotations(newAnnotations);
    }

    private NodeList<AnnotationExpr> computeClassAnnotations(ClassOrInterfaceDeclaration classDeclaration) {
        NodeList<AnnotationExpr> annotations = classDeclaration.getAnnotations();

        NodeList<AnnotationExpr> replacements = new NodeList<>();

        for (AnnotationExpr annotation : annotations) {
            String name = annotation.getNameAsString();

            // @RunWith annotation
            if (RunWith.class.getSimpleName().equals(name)) {
                ClassExpr runnerClass = annotation.asSingleMemberAnnotationExpr().getMemberValue().asClassExpr();
                ClassExpr extensionClass = elementFactories.classExprFactory().extensionClass(runnerClass);

                if (runnerClass.equals(extensionClass)) {
                    // Unfixable - unknown Runner -> Extension mapping
                    commentRegistry.registerUnfixable(classDeclaration, new UnknownRunnerClass(runnerClass));
                    continue;
                }

                replacements.add(elementFactories.annotationExprFactory().extendWith(extensionClass));

                if (runnerClass.getType().asString().contains(MockitoJUnitRunner.Silent.class.getSimpleName())) {
                    replacements.add(elementFactories.annotationExprFactory().mockitoSettings(Strictness.LENIENT));
                    classDeclaration.tryAddImportToParentCompilationUnit(MockitoSettings.class);
                    classDeclaration.tryAddImportToParentCompilationUnit(Strictness.class);
                }
            } else if (Ignore.class.getSimpleName().equals(name)) {
                replacements.add(elementFactories.annotationExprFactory().disabled(annotation));
            } else {
                // Leave all other annotations untouched
                replacements.add(annotation);
            }
        }

        appendFrameworkSpecificChanges(classDeclaration, replacements);

        return Annotations.collapse(replacements);
    }

    private void removeExtendsTestCase(ClassOrInterfaceDeclaration classDeclaration) {
        classDeclaration.setExtendedTypes(
            classDeclaration.getExtendedTypes()
                .stream()
                .filter(type -> !TestCase.class.getSimpleName().equals(type.asString()))
                .collect(Collectors.toCollection(NodeList::new))
        );
    }

    private void appendFrameworkSpecificChanges(ClassOrInterfaceDeclaration classDeclaration, NodeList<AnnotationExpr> replacements) {
        classDeclaration.findCompilationUnit().ifPresent(cu -> {
            Set<Framework> frameworksInUse = frameworkDetectors.frameworksInUse(cu);
            if (frameworksInUse.contains(Framework.MOCKITO)) {
                replacements.add(
                    elementFactories.annotationExprFactory().extendWith(
                        new ClassExpr(StaticJavaParser.parseType(MockitoExtension.class.getSimpleName()))
                    )
                );
                classDeclaration.tryAddImportToParentCompilationUnit(MockitoExtension.class);
            }

            if (frameworksInUse.contains(Framework.WIREMOCK)
                && StaticConfiguration.get().isAttemptWireMockMigration()) {
                String[] canonicalClassName = StaticConfiguration.get()
                    .getWireMockExtension()
                    .split("\\.");
                replacements.add(
                    elementFactories.annotationExprFactory().extendWith(
                        new ClassExpr(StaticJavaParser.parseType(canonicalClassName[canonicalClassName.length - 1]))
                    )
                );
                cu.addImport(StaticConfiguration.get().getWireMockExtension());
                commentRegistry.registerUnfixable(classDeclaration, new WireMockSetUp());
            }
        });

        classDeclaration.findCompilationUnit().ifPresent(cu -> {
            if (frameworkDetectors.frameworksInUse(cu).contains(Framework.MOCKITO)) {
                replacements.add(
                    elementFactories.annotationExprFactory().extendWith(
                        new ClassExpr(StaticJavaParser.parseType(MockitoExtension.class.getSimpleName()))
                    )
                );
                classDeclaration.tryAddImportToParentCompilationUnit(MockitoExtension.class);
            }
        });
    }
}
