package uk.co.paulbenn.jupiterify.service.detector.mockito;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.AnnotationExpr;
import org.mockito.Mock;
import org.springframework.stereotype.Component;

@Component
public class HasMockAnnotation implements MockitoPredicate {

    @Override
    public boolean test(CompilationUnit compilationUnit) {
        return compilationUnit.findFirst(
            AnnotationExpr.class,
            a -> Mock.class.getSimpleName().equals(a.getNameAsString())
        ).isPresent();
    }
}
