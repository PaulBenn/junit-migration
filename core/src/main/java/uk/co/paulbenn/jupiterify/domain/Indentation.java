package uk.co.paulbenn.jupiterify.domain;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
public class Indentation {
    private final Whitespace whitespace;
    private final int amountPerIndent;

    public static Indentation of(Whitespace whitespace, int amountPerIndent) {
        return new Indentation(whitespace, amountPerIndent);
    }

    public static Indentation spaces(int amountPerIndent) {
        return new Indentation(Whitespace.SPACES, amountPerIndent);
    }

    public static Indentation tabs(int amountPerIndent) {
        return new Indentation(Whitespace.TABS, amountPerIndent);
    }

    public String applyIndent(String line, int times) {
        return String.valueOf(whitespace.getWhitespaceCharacter()).repeat(amountPerIndent * times) + line;
    }

    public enum Whitespace {
        SPACES(' '),
        TABS('\t');

        private final char whitespaceCharacter;

        Whitespace(char whitespaceCharacter) {
            this.whitespaceCharacter = whitespaceCharacter;
        }

        public char getWhitespaceCharacter() {
            return whitespaceCharacter;
        }
    }
}
