package uk.co.paulbenn.jupiterify.config;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public final class ConfigurationOptions {

    private ConfigurationOptions() {}

    public static final Option DRY_RUN = Option.builder("d")
        .required(false)
        .numberOfArgs(0)
        .desc("Dry Run")
        .longOpt("dry-run")
        .build();
    public static final Option PROJECT_ROOT = Option.builder("p")
        .required(true)
        .numberOfArgs(1)
        .desc("Project Directory")
        .longOpt("project")
        .build();
    public static final Option SOURCE_SETS = Option.builder("s")
        .required(false)
        .numberOfArgs(1)
        .desc("Source Sets")
        .longOpt("source-sets")
        .build();
    public static final Option OUTPUT_DIR = Option.builder("o")
        .required(false)
        .numberOfArgs(1)
        .desc("Output Directory")
        .longOpt("output-dir")
        .build();
    public static final Option WIREMOCK = Option.builder("w")
        .required(false)
        .numberOfArgs(1)
        .desc("Wiremock extension canonical class name")
        .longOpt("wiremock")
        .build();
    public static final Option CUSTOM_PROPERTY = Option.builder("X")
        .longOpt("X")
        .argName("property=value")
        .numberOfArgs(2)
        .valueSeparator()
        .desc("Assign 'property' to 'value'")
        .build();

    public static Options all() {
        Options options = new Options();
        options.addOption(DRY_RUN);
        options.addOption(PROJECT_ROOT);
        options.addOption(SOURCE_SETS);
        options.addOption(OUTPUT_DIR);
        options.addOption(WIREMOCK);
        options.addOption(CUSTOM_PROPERTY);
        return options;
    }
}
