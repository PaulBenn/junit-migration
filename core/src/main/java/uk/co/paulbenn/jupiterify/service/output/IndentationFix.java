package uk.co.paulbenn.jupiterify.service.output;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;

import java.util.regex.Pattern;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class IndentationFix implements OutputCorrection {

    private static final Pattern CLASS_OR_INTERFACE_DECLARATION
        = Pattern.compile("\\s*(public\\s+)?(class|interface).*");

    private static final Pattern COMMENT_START = Pattern.compile("\\s*/((\\*(\\*)?)|(/)).*");

    // Values from Configuration
    private Pattern indentedLine;
    private String expectedMinimumIndent;

    @Override
    public String applyCorrection(String jupiterJava, JupiterMigration jupiterMigration) {
        return jupiterJava;
//        String[] lines = jupiterJava.split("\\R");
//
//        for (int i = lines.length - 1; i > 1; i--) {
//            String currentLine = lines[i];
//
//            if (CLASS_OR_INTERFACE_DECLARATION.matcher(currentLine).matches()) {
//                break;
//            }
//
//            if (!indentedLine().matcher(currentLine).matches() && !currentLine.matches("}\\s*")) {
//                lines[i] = currentLine.isBlank() ? currentLine : expectedMinimumIndent() + currentLine;
//                currentLine = lines[i];
//            }
//
//            String previousLine = lines[i - 1];
//
//            if (previousLine.matches("\\s*@.*")) {
//                int minimumIndent = computeMinimumIndent(previousLine, currentLine);
//                // Annotations should always match next line's indent
//                String indent = " ".repeat(minimumIndent);
//                lines[i - 1] = indent + previousLine.trim();
//            } else if (COMMENT_START.matcher(previousLine).matches()) {
//                int minimumIndent = computeMinimumIndent(previousLine, currentLine);
//                // Comments should always match the commented node's indent
//                String indent = " ".repeat(Math.max(
//                    StaticConfiguration.get().getIndent(),
//                    minimumIndent - 1
//                ));
//                lines[i - 1] = indent + previousLine.trim();
//            }
//        }
//
//        return String.join(System.lineSeparator(), lines);
    }

    private int computeMinimumIndent(String firstLine, String secondLine) {
        int indentA = computeIndent(firstLine);
        int indentB = computeIndent(secondLine);
        return Math.max(
            StaticConfiguration.get().getIndent(),
            Math.min(indentA, indentB)
        );
    }

    private int computeIndent(String line) {
        return Math.toIntExact(line
            .chars()
            .takeWhile(c -> c == ' ')
            .count()
        );
    }

    private Pattern indentedLine() {
        if (indentedLine == null) {
            indentedLine = Pattern.compile("(( {" + StaticConfiguration.get().getIndent() + "})|\\t).*");
        }

        return indentedLine;
    }

    private String expectedMinimumIndent() {
        if (expectedMinimumIndent == null) {
            expectedMinimumIndent = " ".repeat(StaticConfiguration.get().getIndent());
        }
        return expectedMinimumIndent;
    }
}
