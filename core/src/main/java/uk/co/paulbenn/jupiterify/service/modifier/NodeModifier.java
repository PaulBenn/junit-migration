package uk.co.paulbenn.jupiterify.service.modifier;

import com.github.javaparser.ast.Node;

public interface NodeModifier<E extends Node> {

    boolean canModify(E node);

    default void modifyInPlace(E node) {
        // do nothing
    }

    static <E extends Node> NodeModifier<E> noOp() {
        return NoOpModifier.instance();
    }
}
