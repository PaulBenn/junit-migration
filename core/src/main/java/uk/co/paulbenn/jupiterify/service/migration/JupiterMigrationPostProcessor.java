package uk.co.paulbenn.jupiterify.service.migration;

import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.paulbenn.jupiterify.service.output.OutputCorrection;

import java.util.List;

@Service
@RequiredArgsConstructor
public class JupiterMigrationPostProcessor {

    private final List<OutputCorrection> outputCorrections;

    public void postProcess(JupiterMigration jupiterMigration) {
        jupiterMigration.setJupiterJava(LexicalPreservingPrinter.print(jupiterMigration.getAbstractSyntaxTree()));

        jupiterMigration.setPostProcessedJupiterJava(jupiterMigration.getJupiterJava());

        for (OutputCorrection outputCorrection : outputCorrections) {
            outputCorrection.accept(jupiterMigration);
        }
    }
}
