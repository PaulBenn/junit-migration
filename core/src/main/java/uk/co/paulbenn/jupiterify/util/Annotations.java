package uk.co.paulbenn.jupiterify.util;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Annotations {

    // @Repeatable, single-member annotations
    private static final List<Class<? extends Annotation>> COLLAPSIBLE = List.of(
        ExtendWith.class
    );

    public static Expression getValue(AnnotationExpr annotationExpr) {
        if (annotationExpr.isMarkerAnnotationExpr()) {
            return null;
        }

        if (annotationExpr.isSingleMemberAnnotationExpr()) {
            return annotationExpr.asSingleMemberAnnotationExpr().getMemberValue();
        }

        if (annotationExpr.isNormalAnnotationExpr() && annotationExpr.asNormalAnnotationExpr().getPairs().isNonEmpty()) {
            MemberValuePair keyValuePair = annotationExpr.asNormalAnnotationExpr().getPairs().get(0);
            if ("value".equals(keyValuePair.getNameAsString())) {
                return keyValuePair.getValue();
            }
        }

        return null;
    }

    public static NodeList<AnnotationExpr> collapse(NodeList<AnnotationExpr> annotations) {
        NodeList<AnnotationExpr> distinctAnnotations = annotations.stream()
            .distinct()
            .collect(Collectors.toCollection(NodeList::new));

        // Cannot chain the stream here, as it will apply distinct() just before terminal operation

        Map<Name, AnnotationExpr> collapsed = new HashMap<>();

        for (AnnotationExpr distinct : distinctAnnotations) {
            if (isCollapsible(distinct)) {
                collapsed.merge(
                    distinct.getName(),
                    distinct,
                    (a1, a2) -> {
                        SingleMemberAnnotationExpr existingAnnotation = a1.asSingleMemberAnnotationExpr();
                        SingleMemberAnnotationExpr newAnnotation = a2.asSingleMemberAnnotationExpr();
                        existingAnnotation.setMemberValue(
                            collapseToArray(
                                existingAnnotation.getMemberValue(),
                                newAnnotation.getMemberValue()
                            )
                        );
                        return existingAnnotation;
                    });
            } else {
                collapsed.put(distinct.getName(), distinct);
            }
        }

        return collapsed.values().stream().collect(Collectors.toCollection(NodeList::new));
    }

    private static boolean isCollapsible(AnnotationExpr annotation) {
        for (Class<? extends Annotation> repeatable : COLLAPSIBLE) {
            if (repeatable.getSimpleName().equals(annotation.getNameAsString())) {
                return true;
            }
        }
        return false;
    }

    private static ArrayInitializerExpr collapseToArray(Expression first, Expression second) {
        if (first.isArrayInitializerExpr() && second.isArrayInitializerExpr()) {
            return collapseToArray(first.asArrayInitializerExpr(), second.asArrayInitializerExpr());
        } else if (first.isClassExpr() && second.isClassExpr()) {
            return collapseToArray(first.asClassExpr(), second.asClassExpr());
        } else if (first.isArrayInitializerExpr() && second.isClassExpr()) {
            return collapseToArray(first.asArrayInitializerExpr(), second.asClassExpr());
        } else if (first.isClassExpr() && second.isArrayInitializerExpr()) {
            return collapseToArray(second.asArrayInitializerExpr(), first.asClassExpr());
        } else {
            ArrayInitializerExpr combined = new ArrayInitializerExpr();
            NodeList<Expression> values = new NodeList<>();
            values.add(first);
            values.add(second);
            combined.setValues(values);
            return combined;
        }
    }

    private static ArrayInitializerExpr collapseToArray(ArrayInitializerExpr existing, ArrayInitializerExpr more) {
        NodeList<Expression> existingClasses = existing.getValues();
        existingClasses.addAll(more.getValues());
        existing.setValues(existingClasses);
        return existing;
    }

    private static ArrayInitializerExpr collapseToArray(ArrayInitializerExpr existing, ClassExpr other) {
        NodeList<Expression> existingClasses = existing.getValues();
        existingClasses.add(other);
        existing.setValues(existingClasses);
        return existing;
    }

    private static ArrayInitializerExpr collapseToArray(ClassExpr first, ClassExpr second) {
        ArrayInitializerExpr arrayInitializer = new ArrayInitializerExpr();

        NodeList<Expression> classes = new NodeList<>();
        classes.add(first);
        classes.add(second);

        arrayInitializer.setValues(classes);

        return arrayInitializer;
    }
}
