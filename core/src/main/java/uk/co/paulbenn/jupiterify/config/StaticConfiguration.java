package uk.co.paulbenn.jupiterify.config;

public final class StaticConfiguration {

    private StaticConfiguration() {}

    private static Configuration configuration;

    public static Configuration get() {
        if (configuration == null) {
            throw new IllegalStateException("attempted to access uninitialized configuration");
        }

        return configuration;
    }

    public static void set(Configuration configuration) {
        if (StaticConfiguration.configuration != null) {
            throw new IllegalStateException("attempted to reassign existing configuration");
        }

        StaticConfiguration.configuration = configuration;
    }

    public static void clear() {
        StaticConfiguration.configuration = null;
    }
}
