package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.stereotype.Component;

@Component
public class BeforeClassMethodModifier extends MethodModifier {

    @Override
    public boolean canModify(MethodDeclaration methodDeclaration) {
        return methodDeclaration.getAnnotationByClass(BeforeClass.class).isPresent();
    }

    @Override
    protected NodeList<Modifier> newModifiers(MethodDeclaration methodDeclaration) {
        return new NodeList<>(Modifier.staticModifier());
    }

    @Override
    public NodeList<AnnotationExpr> newAnnotations(MethodDeclaration methodDeclaration) {
        return new NodeList<>(
            new MarkerAnnotationExpr(BeforeAll.class.getSimpleName())
        );
    }
}
