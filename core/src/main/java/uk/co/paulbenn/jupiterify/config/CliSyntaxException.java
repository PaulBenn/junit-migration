package uk.co.paulbenn.jupiterify.config;

import org.apache.commons.cli.Option;

public class CliSyntaxException extends RuntimeException {

    public CliSyntaxException(Option option, String message) {
        super(describe(option) + ": " + message);
    }

    public CliSyntaxException(Option option, String message, Throwable throwable) {
        super(describe(option) + ": " + message, throwable);
    }

    private static String describe(Option option) {
        String key = (option.getLongOpt() != null) ? option.getLongOpt() : option.getOpt();
        return "CLI option '-" + key + "'";
    }
}
