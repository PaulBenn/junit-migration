package uk.co.paulbenn.jupiterify.service;

import uk.co.paulbenn.jupiterify.domain.Indentation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IndentationDetector {

    private static final Pattern SPACES = Pattern.compile("( *)");
    private static final Pattern TABS = Pattern.compile("(\t*)");

    public Indentation inferIndentation(String source) {
        String[] lines = source.split("\\R");

        List<Integer> indentationPerLevel = new ArrayList<>();
        Map<Indentation.Whitespace, Integer> whitespaceCount = new HashMap<>();

        for (String line : lines) {
            if (line.length() == 0) {
                continue;
            }

            Matcher matcher;
            if (line.charAt(0) == ' ') {
                matcher = SPACES.matcher(line);
                whitespaceCount.merge(Indentation.Whitespace.SPACES, 1, Integer::sum);
            } else if (line.charAt(0) == '\t') {
                matcher = TABS.matcher(line);
                whitespaceCount.merge(Indentation.Whitespace.TABS, 1, Integer::sum);
            } else {
                continue;
            }

            // first match only
            if (matcher.find()) {
                indentationPerLevel.add(matcher.group(1).length());
            }
        }

        Indentation.Whitespace inferredWhitespace = inferWhitespace(whitespaceCount);
        int inferredIndentationPerLevel = inferIndentationPerLevel(indentationPerLevel);

        return Indentation.of(inferredWhitespace, inferredIndentationPerLevel);
    }

    private Indentation.Whitespace inferWhitespace(Map<Indentation.Whitespace, Integer> whitespaceCount) {
        int spacesOccurrences = whitespaceCount.getOrDefault(Indentation.Whitespace.SPACES, 0);
        int tabOccurrences = whitespaceCount.getOrDefault(Indentation.Whitespace.TABS, 0);

        return spacesOccurrences >= tabOccurrences ? Indentation.Whitespace.SPACES : Indentation.Whitespace.TABS;
    }

    private int inferIndentationPerLevel(List<Integer> indentationsPerLevel) {
        indentationsPerLevel.sort(Comparator.naturalOrder());

        Map<Integer, Integer> increaseAmounts = new HashMap<>();

        for (int i = 0; i < indentationsPerLevel.size() - 1; i++) {
            int current = indentationsPerLevel.get(i);
            int next = indentationsPerLevel.get(i + 1);
            int increase = next - current;
            if (increase != 0) {
                increaseAmounts.merge(increase, 1, Integer::sum);
            }
        }

        int increase = 0;
        int max = 0;
        for (Map.Entry<Integer, Integer> entry : increaseAmounts.entrySet()) {
            if (entry.getValue() > max) {
                increase = entry.getKey();
                max = entry.getValue();
            }
        }

        return increase;
    }
}
