package uk.co.paulbenn.jupiterify.util;

public class ReflectiveException extends RuntimeException {
    public ReflectiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
