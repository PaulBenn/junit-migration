package uk.co.paulbenn.jupiterify.service.migration;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class JupiterMigrationPreProcessor {

    private static final Pattern ANNOTATION_PLUS_COMMENT = Pattern.compile(
        "(?:@\\w+?)(?:\\(.*?\\))?(\\s*/(?:/|\\*{1,2}).*)\\R\\s*.*?void\\s*(.*)\\("
    );

    public void preProcess(JupiterMigration jupiterMigration) {
        Map<String, String> commentedAnnotations = new HashMap<>();

        Matcher matcher = ANNOTATION_PLUS_COMMENT.matcher(jupiterMigration.getJUnit4Java());

        Map<String, String> replacements = new HashMap<>();

        while (matcher.find()) {
            replacements.put(matcher.group(), matcher.group().replace(matcher.group(1), ""));
            commentedAnnotations.put(matcher.group(2), matcher.group(1));
        }

        String preprocessedJUnit4Java = jupiterMigration.getJUnit4Java();
        for (Map.Entry<String, String> match : replacements.entrySet()) {
            preprocessedJUnit4Java = preprocessedJUnit4Java.replace(match.getKey(), match.getValue());
        }

        jupiterMigration.setCommentsAfterAnnotations(commentedAnnotations);
        jupiterMigration.setPreProcessedJUnit4Java(preprocessedJUnit4Java);
    }
}
