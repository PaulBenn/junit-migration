package uk.co.paulbenn.jupiterify.service.modifier;

import com.github.javaparser.ast.body.FieldDeclaration;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
public class FieldModifiers {

    private final List<NodeModifier<FieldDeclaration>> fieldModifiers;

    public FieldModifiers(List<NodeModifier<FieldDeclaration>> fieldModifiers) {
        this.fieldModifiers = fieldModifiers;
    }

    public Stream<NodeModifier<FieldDeclaration>> allThatCanModify(FieldDeclaration fieldDeclaration) {
        return fieldModifiers.stream().filter(modifier -> modifier.canModify(fieldDeclaration));
    }
}
