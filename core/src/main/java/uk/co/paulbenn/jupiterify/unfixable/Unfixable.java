package uk.co.paulbenn.jupiterify.unfixable;

public interface Unfixable {
    String toDoStatement();
}
