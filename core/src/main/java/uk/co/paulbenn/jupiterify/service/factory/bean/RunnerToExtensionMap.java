package uk.co.paulbenn.jupiterify.service.factory.bean;

import org.junit.jupiter.api.extension.Extension;
import org.junit.runner.Runner;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RunnerToExtensionMap {

    @Bean
    public Map<Class<? extends Runner>, Class<? extends Extension>> runnerToExtension() {
        Map<Class<? extends Runner>, Class<? extends Extension>> runnerToExtension = new HashMap<>();

        runnerToExtension.put(MockitoJUnitRunner.class, MockitoExtension.class);
        runnerToExtension.put(MockitoJUnitRunner.Silent.class, MockitoExtension.class);
        runnerToExtension.put(org.mockito.runners.MockitoJUnitRunner.class, MockitoExtension.class);
        runnerToExtension.put(org.mockito.runners.MockitoJUnitRunner.Silent.class, MockitoExtension.class);
        runnerToExtension.put(SpringRunner.class, SpringExtension.class);
        runnerToExtension.put(SpringJUnit4ClassRunner.class, SpringExtension.class);

        return runnerToExtension;
    }

}
