package uk.co.paulbenn.jupiterify.service.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.paulbenn.jupiterify.service.CommentRegistry;
import uk.co.paulbenn.jupiterify.service.visitor.VisitorChain;

@Service
@RequiredArgsConstructor
public class JupiterMigrationService {

    private final JupiterMigrationPreProcessor jupiterMigrationPreProcessor;
    private final JupiterMigrationParser jupiterMigrationParser;
    private final VisitorChain<CommentRegistry> visitorChain;
    private final JupiterMigrationPostProcessor jupiterMigrationPostProcessor;

    public String migrate(JupiterMigration jupiterMigration) {
        jupiterMigrationPreProcessor.preProcess(jupiterMigration);
        jupiterMigrationParser.parse(jupiterMigration);
        visitorChain.visit(jupiterMigration.getAbstractSyntaxTree());
        jupiterMigrationPostProcessor.postProcess(jupiterMigration);

        return jupiterMigration.getPostProcessedJupiterJava();
    }
}
