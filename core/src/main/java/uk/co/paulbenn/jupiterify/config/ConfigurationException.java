package uk.co.paulbenn.jupiterify.config;

public class ConfigurationException extends RuntimeException {

    public ConfigurationException(String[] args, String message, Throwable cause) {
        super(
            message + " (args = '" + String.join(" ", args) + "')",
            cause
        );
    }
}
