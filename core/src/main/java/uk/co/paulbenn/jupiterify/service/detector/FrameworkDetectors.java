package uk.co.paulbenn.jupiterify.service.detector;

import com.github.javaparser.ast.CompilationUnit;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FrameworkDetectors {

    private final List<FrameworkDetector> frameworkDetectors;

    public FrameworkDetectors(List<FrameworkDetector> frameworkDetectors) {
        this.frameworkDetectors = frameworkDetectors;
    }

    public Set<Framework> frameworksInUse(CompilationUnit compilationUnit) {
        Set<Framework> frameworksInUse = new HashSet<>();

        for (FrameworkDetector frameworkDetector : frameworkDetectors) {
            if (frameworkDetector.test(compilationUnit)) {
                frameworksInUse.add(frameworkDetector.supportedFramework());
            }
        }

        return frameworksInUse;
    }
}
