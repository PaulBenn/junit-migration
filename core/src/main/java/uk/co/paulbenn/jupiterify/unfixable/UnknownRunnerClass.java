package uk.co.paulbenn.jupiterify.unfixable;

import com.github.javaparser.ast.expr.ClassExpr;

public class UnknownRunnerClass implements Unfixable {

    private final String runnerClass;

    public UnknownRunnerClass(ClassExpr runnerClass) {
        this.runnerClass = runnerClass.getType().asString();
    }

    @Override
    public String toDoStatement() {
        return "TODO"
            + System.lineSeparator()
            + "The JUnit 4 @RunWith annotation contains an irreplaceable value:"
            + System.lineSeparator()
            + System.lineSeparator()
            + "    @RunWith(" + runnerClass + ".class)"
            + System.lineSeparator()
            + System.lineSeparator()
            + "Class<? extends Runner> value '"
            + runnerClass
            + "'"
            + System.lineSeparator()
            + "does not have a known equivalent Class<? extends Extension> in JUnit 5."
            + System.lineSeparator();
    }
}
