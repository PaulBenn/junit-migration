package uk.co.paulbenn.jupiterify.service;

import com.github.javaparser.ast.Node;
import uk.co.paulbenn.jupiterify.unfixable.Unfixable;
import uk.co.paulbenn.jupiterify.util.DataKeys;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class CommentRegistry {

    private final Map<UUID, String> comments = new HashMap<>();

    public String getComment(Node n) {
        return comments.get(n.getData(DataKeys.UUID()));
    }

    public void registerUnfixable(Node n, Unfixable unfixable) {
        registerComment(n, unfixable.toDoStatement());
    }

    private void registerComment(Node n, String comment) {
        if (n.containsData(DataKeys.UUID())) {
            comments.merge(
                n.getData(DataKeys.UUID()),
                comment,
                (comment1, comment2) -> comment1 + System.lineSeparator() + comment2
            );
        } else {
            UUID uuid = UUID.randomUUID();
            n.setData(DataKeys.UUID(), uuid);
            comments.put(uuid, comment);
        }
    }
}
