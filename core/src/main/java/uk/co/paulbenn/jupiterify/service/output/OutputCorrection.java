package uk.co.paulbenn.jupiterify.service.output;

import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;

import java.util.function.Consumer;

public interface OutputCorrection extends Consumer<JupiterMigration> {

    @Override
    default void accept(JupiterMigration jupiterMigration) {
        String jupiterJava = jupiterMigration.getPostProcessedJupiterJava();

        jupiterJava = applyCorrection(jupiterJava, jupiterMigration);

        jupiterMigration.setPostProcessedJupiterJava(jupiterJava);
    }

    String applyCorrection(String jupiterJava, JupiterMigration jupiterMigration);
}
