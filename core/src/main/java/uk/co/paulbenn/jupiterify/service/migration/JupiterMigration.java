package uk.co.paulbenn.jupiterify.service.migration;

import com.github.javaparser.ast.CompilationUnit;
import lombok.Getter;
import lombok.Setter;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class JupiterMigration {

    private Path pathToJUnit4Java;
    private String jUnit4Java;
    private String preProcessedJUnit4Java;

    private CompilationUnit abstractSyntaxTree;

    private Path pathToJupiterJava;
    private String jupiterJava;
    private String postProcessedJupiterJava;

    // K = method name (assumed to be unique... yes this is horrible)
    // V = any content after the annotation, including spaces and the comment
    // e.g. for:
    // @Test // my comment
    // public void testMethodName() {
    // }
    // K = "testMethodName", V = " // my comment"
    private Map<String, String> commentsAfterAnnotations = new HashMap<>();

    public JupiterMigration(Path pathToJUnit4Java, String jUnit4Java) {
        this.pathToJUnit4Java = pathToJUnit4Java;
        this.jUnit4Java = jUnit4Java;
    }
}
