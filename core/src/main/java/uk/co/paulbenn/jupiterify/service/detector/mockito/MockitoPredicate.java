package uk.co.paulbenn.jupiterify.service.detector.mockito;

import com.github.javaparser.ast.CompilationUnit;

import java.util.function.Predicate;

public interface MockitoPredicate extends Predicate<CompilationUnit> {

}
