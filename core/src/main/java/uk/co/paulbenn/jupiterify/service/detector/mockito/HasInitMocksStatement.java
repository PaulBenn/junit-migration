package uk.co.paulbenn.jupiterify.service.detector.mockito;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MethodCallExpr;
import org.springframework.stereotype.Component;

@Component
public class HasInitMocksStatement implements MockitoPredicate {

    @Override
    public boolean test(CompilationUnit compilationUnit) {
        return compilationUnit.findFirst(
            MethodCallExpr.class,
            a -> "initMocks".equals(a.getNameAsString())
        ).isPresent();
    }
}
