package uk.co.paulbenn.jupiterify.service;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.observer.AstObserver;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import uk.co.paulbenn.jupiterify.util.ReflectiveException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Service
public class ObserverDeDuplicator {

    // TODO this class may not be needed as per change from List<AstObserver> to Set<AstObserver>
    public void deDuplicateObservers(Node node) {
        Set<AstObserver> observers = reflectivelyAccessObservers(node);

        List<AstObserver> shallowCopy = new ArrayList<>(observers);

        // Direct use of Node::unregister will produce CME
        for (Iterator<AstObserver> it = observers.iterator(); it.hasNext(); ) {
            it.next();
            it.remove();
        }

        Set<AstObserver> uniqueObservers = new HashSet<>(shallowCopy);

        for (AstObserver observer : uniqueObservers) {
            node.register(observer);
        }
    }

    private Set<AstObserver> reflectivelyAccessObservers(Node node) {
        try {
            Field observers = Node.class.getDeclaredField("observers");
            ReflectionUtils.makeAccessible(observers);

            Object rawObservers = ReflectionUtils.getField(observers, node);

            @SuppressWarnings("unchecked")
            Set<AstObserver> typedObservers = (Set<AstObserver>) rawObservers;

            return typedObservers;
        } catch (NoSuchFieldException e) {
            throw new ReflectiveException("field 'observers' not found", e);
        }
    }
}
