package uk.co.paulbenn.jupiterify.service.output;

import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;

public class NewLineAtEndOfFile implements OutputCorrection {

    @Override
    public String applyCorrection(String jupiterJava, JupiterMigration jupiterMigration) {
        if (jupiterJava.endsWith(System.lineSeparator())) {
            jupiterJava += System.lineSeparator();
        }
        return jupiterJava;
    }
}
