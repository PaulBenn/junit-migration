package uk.co.paulbenn.jupiterify.service.visitor;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import uk.co.paulbenn.jupiterify.service.modifier.ClassDeclarationModifier;
import uk.co.paulbenn.jupiterify.service.modifier.FieldModifiers;
import uk.co.paulbenn.jupiterify.service.modifier.imports.ImportModifiers;
import uk.co.paulbenn.jupiterify.service.modifier.methods.MethodModifiers;
import uk.co.paulbenn.jupiterify.service.CommentRegistry;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(1)
public class MigratingVisitor extends ModifierVisitor<CommentRegistry> {

    private final ImportModifiers importModifiers;
    private final ClassDeclarationModifier classDeclarationModifier;
    private final FieldModifiers fieldModifiers;
    private final MethodModifiers methodModifiers;

    public MigratingVisitor(ImportModifiers importModifiers,
                            ClassDeclarationModifier classDeclarationModifier,
                            FieldModifiers fieldModifiers,
                            MethodModifiers methodModifiers) {
        this.importModifiers = importModifiers;
        this.classDeclarationModifier = classDeclarationModifier;
        this.fieldModifiers = fieldModifiers;
        this.methodModifiers = methodModifiers;
    }

    @Override
    public Node visit(ImportDeclaration importDeclaration, CommentRegistry commentRegistry) {
        importModifiers.getModifier(importDeclaration).modifyInPlace(importDeclaration);

        return super.visit(importDeclaration, commentRegistry);
    }

    @Override
    public Visitable visit(ClassOrInterfaceDeclaration classDeclaration, CommentRegistry commentRegistry) {
        classDeclarationModifier.modifyInPlace(classDeclaration);

        return super.visit(classDeclaration, commentRegistry);
    }

    @Override
    public Visitable visit(FieldDeclaration fieldDeclaration, CommentRegistry commentRegistry) {
        fieldModifiers.allThatCanModify(fieldDeclaration)
            .forEach(modifier -> modifier.modifyInPlace(fieldDeclaration));

        return super.visit(fieldDeclaration, commentRegistry);
    }

    @Override
    public Visitable visit(MethodDeclaration methodDeclaration, CommentRegistry commentRegistry) {
        methodModifiers.allThatCanModify(methodDeclaration)
            .forEach(modifier -> modifier.modifyInPlace(methodDeclaration));

        return super.visit(methodDeclaration, commentRegistry);
    }
}
