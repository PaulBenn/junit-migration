package uk.co.paulbenn.jupiterify.service;

import uk.co.paulbenn.jupiterify.util.PathVerifier;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SourceCodeIO {

    public static String readSource(Path path) {
        PathVerifier.verifyExists(path);
        PathVerifier.verifyIsRegularFile(path);
        PathVerifier.verifyIsReadable(path);
        PathVerifier.verifyExtension(path, ".java");

        try {
            return Files.readString(path);
        } catch (IOException e) {
            throw new UncheckedIOException("cannot read source code from path " + path, e);
        }
    }

    public static void writeSource(Path path, String sourceCode) {
        try {
            Files.writeString(path, sourceCode);
        } catch (IOException e) {
            throw new UncheckedIOException("cannot write source code to path " + path, e);
        }
    }
}
