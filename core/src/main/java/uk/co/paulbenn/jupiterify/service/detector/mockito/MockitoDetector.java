package uk.co.paulbenn.jupiterify.service.detector.mockito;

import com.github.javaparser.ast.CompilationUnit;
import uk.co.paulbenn.jupiterify.service.detector.Framework;
import uk.co.paulbenn.jupiterify.service.detector.FrameworkDetector;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
public class MockitoDetector implements FrameworkDetector {

    private Predicate<CompilationUnit> usesMockito;

    public MockitoDetector(List<MockitoPredicate> mockitoPredicates) {
        this.usesMockito = any -> false;
        for (MockitoPredicate predicate : mockitoPredicates) {
            usesMockito = usesMockito.or(predicate);
        }
    }

    @Override
    public boolean test(CompilationUnit compilationUnit) {
        return usesMockito.test(compilationUnit);
    }

    @Override
    public Framework supportedFramework() {
        return Framework.MOCKITO;
    }
}
