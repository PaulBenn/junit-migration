package uk.co.paulbenn.jupiterify.service.modifier;

import com.github.javaparser.ast.Node;
import org.springframework.stereotype.Component;

@Component
public class NoOpModifier<E extends Node> implements NodeModifier<E> {

    private static final NoOpModifier<?> INSTANCE = new NoOpModifier();

    private NoOpModifier() {
    }

    public static <E extends Node> NoOpModifier<E> instance() {
        @SuppressWarnings("unchecked")
        NoOpModifier<E> typed = (NoOpModifier<E>) INSTANCE;
        return typed;
    }

    @Override
    public final boolean canModify(E expr) {
        return true;
    }
}
