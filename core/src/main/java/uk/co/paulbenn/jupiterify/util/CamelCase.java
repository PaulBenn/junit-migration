package uk.co.paulbenn.jupiterify.util;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.SimpleName;
import org.springframework.util.StringUtils;

public class CamelCase {

    public static SimpleName toCamelCase(SimpleName simpleName) {
        return StaticJavaParser.parseSimpleName(toCamelCase(simpleName.asString()));
    }

    private static String toCamelCase(String s) {
        if (s.endsWith("_")) {
            s = s.substring(0, s.length() - 1);
        }

        StringBuilder camelCase = new StringBuilder();

        String[] partsByUnderScore = s.split("_");
        for (int i = 0; i < partsByUnderScore.length; i++) {
            if (!partsByUnderScore[i].isBlank()) {
                camelCase.append(
                    i == 0
                        ? partsByUnderScore[i]
                        : StringUtils.capitalize(partsByUnderScore[i])
                );
            }
        }

        return camelCase.toString();
    }
}
