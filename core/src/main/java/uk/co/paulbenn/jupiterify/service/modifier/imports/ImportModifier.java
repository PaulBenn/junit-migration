package uk.co.paulbenn.jupiterify.service.modifier.imports;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.Name;
import uk.co.paulbenn.jupiterify.service.modifier.NodeModifier;

public class ImportModifier implements NodeModifier<ImportDeclaration> {

    private final Class<?> jUnit4Type;

    private final Class<?> jUnit5Type;

    public ImportModifier(Class<?> jUnit4Type, Class<?> jUnit5Type) {
        this.jUnit4Type = jUnit4Type;
        this.jUnit5Type = jUnit5Type;
    }

    Class<?> getJUnit4Type() {
        return jUnit4Type;
    }

    Class<?> getJUnit5Type() {
        return jUnit5Type;
    }

    @Override
    public boolean canModify(ImportDeclaration importDeclaration) {
        return !importDeclaration.isStatic() && importDeclaration.getNameAsString().equals(jUnit4Type.getCanonicalName());
    }

    @Override
    public void modifyInPlace(ImportDeclaration importDeclaration) {
        Name replacementName = StaticJavaParser.parseName(jUnit5Type.getCanonicalName());
        importDeclaration.getName().replace(replacementName);
    }
}
