package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;
import uk.co.paulbenn.jupiterify.service.modifier.NodeModifier;
import uk.co.paulbenn.jupiterify.util.CamelCase;

public abstract class MethodModifier implements NodeModifier<MethodDeclaration> {

    @Override
    public void modifyInPlace(MethodDeclaration methodDeclaration) {
        if (StaticConfiguration.get().isUseCamelCase()) {
            methodDeclaration.setName(CamelCase.toCamelCase(methodDeclaration.getName()));
        }

        // Printer observers cannot deal with direct "set" calls.

        NodeList<Modifier> newModifiers = newModifiers(methodDeclaration);
        methodDeclaration.getModifiers().removeIf(x -> true);
        methodDeclaration.setModifiers(newModifiers);

        NodeList<AnnotationExpr> newAnnotations = newAnnotations(methodDeclaration);
        methodDeclaration.getAnnotations().removeIf(x -> true);
        methodDeclaration.setAnnotations(newAnnotations);
    }

    protected NodeList<Modifier> newModifiers(MethodDeclaration methodDeclaration) {
        return new NodeList<>();
    }

    protected NodeList<AnnotationExpr> newAnnotations(MethodDeclaration methodDeclaration) {
        return methodDeclaration.getAnnotations();
    }
}
