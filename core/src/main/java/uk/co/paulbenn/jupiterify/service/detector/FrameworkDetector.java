package uk.co.paulbenn.jupiterify.service.detector;

import com.github.javaparser.ast.CompilationUnit;

import java.util.function.Predicate;

public interface FrameworkDetector extends Predicate<CompilationUnit> {

    Framework supportedFramework();
}
