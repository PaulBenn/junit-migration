package uk.co.paulbenn.jupiterify.util;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Comments {

    private static final Pattern LINE_SEPARATOR = Pattern.compile("\\R");

    public static Comment commentForNode(Node node, String content) {
        Optional<Comment> existingComment = node.getComment();

        return existingComment
            .map(value -> updateComment(node, value, content))
            .orElseGet(() -> createComment(node, content));
    }

    private static Comment createComment(Node node, String content) {
        if (LINE_SEPARATOR.matcher(content).find()) {
            return new BlockComment(new BlockCommentFormatter(node).format("", content));
        }
        return new LineComment(content);
    }

    private static Comment updateComment(Node node, Comment existingComment, String content) {
        if (content == null) {
            return existingComment;
        }
        return CommentUpdater.forComment(existingComment).updateComment(node, existingComment, content);
    }

    private static String preContentIndent(String content) {
        String contentWithoutLineSeparators = LINE_SEPARATOR.matcher(content).replaceAll("");
        int[] indentChars = contentWithoutLineSeparators
            .chars()
            .takeWhile(c -> c == ' ')
            .toArray();
        return new String(indentChars, 0, indentChars.length);
    }

    private static String postContentIndent(String content) {
        String contentWithoutLineSeparators = LINE_SEPARATOR.matcher(content).replaceAll("");
        int[] indentChars = new StringBuilder(contentWithoutLineSeparators)
            .reverse()
            .toString()
            .chars()
            .takeWhile(c -> c == ' ')
            .toArray();
        return new String(indentChars, 0, indentChars.length);
    }

    private enum CommentUpdater {
        LINE(Comment::isLineComment) {
            @Override
            protected Comment updateComment(Node node, Comment existingComment, String content) {
                return new BlockComment(
                    new BlockCommentFormatter(node).format(existingComment.getContent(), content)
                );
            }
        },
        BLOCK(Comment::isBlockComment) {
            @Override
            protected Comment updateComment(Node node, Comment existingComment, String content) {
                String existingContent = existingComment.getContent();
                String preContentIndent = preContentIndent(existingContent);
                String postContentIndent = postContentIndent(existingContent);

                return new BlockComment(
                    new BlockCommentFormatter(preContentIndent, postContentIndent).format(existingContent, content)
                );
            }
        },
        JAVADOC(Comment::isJavadocComment) {
            @Override
            protected Comment updateComment(Node node, Comment existingComment, String content) {
                String existingContent = existingComment.getContent();
                String preContentIndent = preContentIndent(existingContent);
                String postContentIndent = postContentIndent(existingContent);

                return new JavadocComment(
                    new BlockCommentFormatter(preContentIndent, postContentIndent).format(existingContent, content)
                );
            }
        };

        private final Predicate<Comment> match;

        CommentUpdater(Predicate<Comment> match) {
            this.match = match;
        }

        static CommentUpdater forComment(Comment comment) {
            for (CommentUpdater commentUpdater : CommentUpdater.values()) {
                if (commentUpdater.matches(comment)) {
                    return commentUpdater;
                }
            }

            throw new IllegalArgumentException("No comment updater for comment type " + comment.getClass());
        }

        protected abstract Comment updateComment(Node node, Comment existingComment, String content);

        private boolean matches(Comment comment) {
            return match.test(comment);
        }
    }

    private static class BlockCommentFormatter {

        private static final String DEFAULT_BLOCK_COMMENT_LINES_INDENT = "     ";

        private final String preContentIndent;
        private final String postContentIndent;

        BlockCommentFormatter(Node node) {
            Optional<Node> parentNode = node.getParentNode();
            if (parentNode.isPresent() && parentNode.get() instanceof CompilationUnit) {
                this.preContentIndent = "";
                this.postContentIndent = "";
            } else {
                this.preContentIndent = DEFAULT_BLOCK_COMMENT_LINES_INDENT;
                this.postContentIndent = DEFAULT_BLOCK_COMMENT_LINES_INDENT;
            }
        }

        BlockCommentFormatter(String preContentIndent, String postContentIndent) {
            this.preContentIndent = preContentIndent;
            this.postContentIndent = postContentIndent;
        }

        String format(String existingContent, String newContent) {
            StringBuilder formattedContent = new StringBuilder();

            formattedContent.append(System.lineSeparator());

            if (!existingContent.isBlank()) {
                String[] existingContentLines = LINE_SEPARATOR.split(existingContent);
                for (String line : existingContentLines) {
                    formattedContent.append(preContentIndent)
                        .append(starredLine(line))
                        .append(System.lineSeparator());
                }
            }

            if (!newContent.isBlank()) {
                String[] newContentLines = LINE_SEPARATOR.split(newContent);
                for (String line : newContentLines) {
                    formattedContent.append(preContentIndent)
                        .append(starredLine(line))
                        .append(System.lineSeparator());
                }
            }

            formattedContent.append(postContentIndent);

            return formattedContent.toString();
        }

        private static String starredLine(String line) {
            String correctStarPrefix = "* ";
            line = line.trim().replaceFirst("\\* +", correctStarPrefix);

            if (!line.startsWith(correctStarPrefix)) {
                line = correctStarPrefix + line;
            }

            return line;
        }
    }
}
