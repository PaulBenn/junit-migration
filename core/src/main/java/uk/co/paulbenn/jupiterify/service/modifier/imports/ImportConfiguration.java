package uk.co.paulbenn.jupiterify.service.modifier.imports;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@Configuration
public class ImportConfiguration {

    @Bean
    public ImportModifier afterClass() {
        return new ImportModifier(AfterClass.class, AfterAll.class);
    }

    @Bean
    public ImportModifier after() {
        return new ImportModifier(After.class, AfterEach.class);
    }

    @Bean
    public ImportModifier beforeClass() {
        return new ImportModifier(BeforeClass.class, BeforeAll.class);
    }

    @Bean
    public ImportModifier before() {
        return new ImportModifier(Before.class, BeforeEach.class);
    }

    @Bean
    public ImportModifier ignore() {
        return new ImportModifier(Ignore.class, Disabled.class);
    }

    @Bean
    public ImportModifier mockitoJUnitRunnerOld() {
        return new ImportModifier(MockitoJUnitRunner.class, MockitoExtension.class);
    }

    @Bean
    public ImportModifier mockitoJUnitRunnerNew() {
        return new ImportModifier(
            org.mockito.junit.MockitoJUnitRunner.class,
            MockitoExtension.class
        );
    }

    @Bean
    public ImportModifier runWith() {
        return new ImportModifier(RunWith.class, ExtendWith.class);
    }

    @Bean
    public ImportModifier springJUnit4ClassRunner() {
        return new ImportModifier(
            SpringJUnit4ClassRunner.class,
            SpringExtension.class
        );
    }

    @Bean
    public ImportModifier springRunner() {
        return new ImportModifier(SpringRunner.class, SpringExtension.class);
    }

    @Bean
    public ImportModifier test() {
        return new ImportModifier(Test.class, org.junit.jupiter.api.Test.class);
    }
}
