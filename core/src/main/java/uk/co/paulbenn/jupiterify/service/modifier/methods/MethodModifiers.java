package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.ast.body.MethodDeclaration;
import uk.co.paulbenn.jupiterify.service.modifier.NodeModifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
public class MethodModifiers {

    private final List<NodeModifier<MethodDeclaration>> methodModifiers;

    public MethodModifiers(List<NodeModifier<MethodDeclaration>> methodModifiers) {
        this.methodModifiers = methodModifiers;
    }

    public Stream<NodeModifier<MethodDeclaration>> allThatCanModify(MethodDeclaration methodDeclaration) {
        return methodModifiers.stream().filter(modifier -> modifier.canModify(methodDeclaration));
    }
}
