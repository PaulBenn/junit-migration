package uk.co.paulbenn.jupiterify.util;

import com.github.javaparser.ast.DataKey;

import java.util.UUID;

public class DataKeys {

    private static final DataKey<UUID> UUID = new DataKey<>() {
    };

    public static DataKey<UUID> UUID() {
        return UUID;
    }
}
