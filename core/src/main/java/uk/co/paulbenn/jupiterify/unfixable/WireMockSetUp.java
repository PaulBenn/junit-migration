package uk.co.paulbenn.jupiterify.unfixable;

import uk.co.paulbenn.jupiterify.config.StaticConfiguration;

public class WireMockSetUp implements Unfixable {

    @Override
    public String toDoStatement() {
        return "TODO"
            + System.lineSeparator()
            + "Since extension '"
            + StaticConfiguration.get().getWireMockExtension()
            + "' is user specified, "
            + System.lineSeparator()
            + "its behaviour is unknown."
            + System.lineSeparator()
            + "Set-up may be required to start and stop WireMockServer instances in this class."
            + System.lineSeparator();
    }
}
