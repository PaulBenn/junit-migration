package uk.co.paulbenn.jupiterify.service.factory.bean;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NameExpr;
import org.mockito.quality.Strictness;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;

@Configuration
public class MockitoSettingsMap {

    @Bean
    public Map<Strictness, MemberValuePair> strictnessPairs() {
        Map<Strictness, MemberValuePair> strictnessPairs = new EnumMap<>(Strictness.class);

        for (Strictness strictness : Strictness.values()) {
            strictnessPairs.put(
                strictness,
                new MemberValuePair(
                    "strictness",
                    new FieldAccessExpr(
                        new NameExpr(StaticJavaParser.parseSimpleName(Strictness.class.getSimpleName())),
                        strictness.name()
                    ))
            );
        }

        return strictnessPairs;
    }
}
