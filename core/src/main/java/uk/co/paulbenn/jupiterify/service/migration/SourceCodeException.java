package uk.co.paulbenn.jupiterify.service.migration;

public class SourceCodeException extends RuntimeException {
    public SourceCodeException(String message) {
        super(message);
    }
}
