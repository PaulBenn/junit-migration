package uk.co.paulbenn.jupiterify.service.modifier.imports;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.Name;

import java.util.Arrays;

public class StaticImportModifier extends ImportModifier {

    public StaticImportModifier(Class<?> jUnit4Type, Class<?> jUnit5Type) {
        super(jUnit4Type, jUnit5Type);
    }

    @Override
    public boolean canModify(ImportDeclaration importDeclaration) {
        if (!importDeclaration.isStatic()) {
            return false;
        }

        String[] parts = importDeclaration.getNameAsString().split("\\.");
        String canonicalName = String.join(".", Arrays.copyOfRange(parts, 0, parts.length - 1));
        return canonicalName.equals(getJUnit4Type().getCanonicalName());
    }

    @Override
    public void modifyInPlace(ImportDeclaration importDeclaration) {
        String[] parts = importDeclaration.getNameAsString().split("\\.");
        Name replacementName = StaticJavaParser.parseName(
            getJUnit5Type().getCanonicalName() + "." + parts[parts.length - 1]
        );
        importDeclaration.getName().replace(replacementName);
    }
}
