package uk.co.paulbenn.jupiterify.config;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.springframework.stereotype.Service;
import uk.co.paulbenn.jupiterify.util.PathVerifier;

import java.nio.file.Path;
import java.util.Optional;

@Service
public class ConfigurationParser {

    private final CommandLineParser commandLineParser;

    public ConfigurationParser() {
        this.commandLineParser = new DefaultParser();
    }

    public Configuration fromCommandLine(String[] args) {
        CommandLine commandLine;

        try {
            commandLine = commandLineParser.parse(ConfigurationOptions.all(), args);
        } catch (ParseException e) {
            throw new ConfigurationException(args, "invalid command-line", e);
        }

        return fromCommandLine(commandLine);
    }

    private Configuration fromCommandLine(CommandLine commandLine) {
        return fromCommandLineWrapper(new CommandLineWrapper(commandLine));
    }

    private Configuration fromCommandLineWrapper(CommandLineWrapper commandLine) throws CliSyntaxException {
        Configuration configuration = new Configuration();

        boolean dryRun = Optional.ofNullable(commandLine.getBoolean(ConfigurationOptions.DRY_RUN))
            .orElse(false);
        configuration.setDryRun(dryRun);

        String[] sourceSets = commandLine.getStrings(ConfigurationOptions.SOURCE_SETS);
        if (sourceSets.length == 0) {
            sourceSets = new String[] { "test" };
        }
        configuration.setSourceSets(sourceSets);

        // required
        Path projectRoot = commandLine.getPath(ConfigurationOptions.PROJECT_ROOT);
        configuration.setProjectRoot(projectRoot);

        Path outputDirectory = Optional.ofNullable(commandLine.getPath(ConfigurationOptions.OUTPUT_DIR))
            .orElse(projectRoot);
        configuration.setOutputDirectory(outputDirectory);

        configuration.setIndent(Integer.parseInt(commandLine.getCustomProperty("tabSize", "4")));
//        configuration.setAttemptWireMockMigration(commandLine.hasOption(ConfigurationOptions.WIREMOCK.getOpt()));
//        configuration.setWireMockExtension(commandLine.getOptionValue(ConfigurationOptions.WIREMOCK.getOpt()));
        configuration.setUseCamelCase(Boolean.parseBoolean(commandLine.getCustomProperty("useCamelCase", "false")));

        validate(configuration);

        return configuration;
    }

    private void validate(Configuration configuration) {
        PathVerifier.verifyExists(configuration.getProjectRoot());
        PathVerifier.verifyIsReadable(configuration.getProjectRoot());
        PathVerifier.verifyIsDirectory(configuration.getProjectRoot());

        PathVerifier.verifyExists(configuration.getOutputDirectory());
        PathVerifier.verifyIsWritable(configuration.getOutputDirectory());
        PathVerifier.verifyIsDirectory(configuration.getOutputDirectory());
    }
}
