package uk.co.paulbenn.jupiterify.service.migration;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import org.springframework.stereotype.Service;

@Service
public class JupiterMigrationParser {

    private final JavaParser javaParser = new JavaParser();

    public void parse(JupiterMigration jupiterMigration) {
        CompilationUnit ast = parseAbstractSyntaxTree(jupiterMigration);

        ast.setStorage(jupiterMigration.getPathToJUnit4Java());

        jupiterMigration.setAbstractSyntaxTree(ast);

        LexicalPreservingPrinter.setup(ast);
    }

    private CompilationUnit parseAbstractSyntaxTree(JupiterMigration jupiterMigration) {
        ParseResult<CompilationUnit> parseResult = javaParser.parse(jupiterMigration.getPreProcessedJUnit4Java());

        if (parseResult.isSuccessful()) {
            return parseResult.getResult().orElseThrow(() -> new SourceCodeException("cannot happen"));
        }

        throw new SourceCodeException("parsing failed for path " + jupiterMigration.getPathToJUnit4Java());
    }
}
