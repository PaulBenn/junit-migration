package uk.co.paulbenn.jupiterify.service.output;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;

import java.util.Map;

@Component
@Order
public class CommentsAfterAnnotations implements OutputCorrection {

    @Override
    public String applyCorrection(String jupiterJava, JupiterMigration jupiterMigration) {
        String[] lines = jupiterJava.split("\\R");
        Map<String, String> commentsAfterAnnotations = jupiterMigration.getCommentsAfterAnnotations();

        for (int i = 0; i < lines.length; i++) {
            for (Map.Entry<String, String> commentAfterAnnotation : commentsAfterAnnotations.entrySet()) {
                if (lines[i].contains(" " + commentAfterAnnotation.getKey())) {
                    // TODO prev index might not be test annotation
                    lines[i - 1] += commentAfterAnnotation.getValue();
                }
            }
        }

        return String.join(System.lineSeparator(), lines);
    }
}
