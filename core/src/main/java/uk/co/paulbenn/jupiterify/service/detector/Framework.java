package uk.co.paulbenn.jupiterify.service.detector;

public enum Framework {
    JUNIT,
    MOCKITO,
    WIREMOCK
}
