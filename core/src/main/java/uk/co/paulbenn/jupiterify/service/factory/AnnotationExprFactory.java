package uk.co.paulbenn.jupiterify.service.factory;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import uk.co.paulbenn.jupiterify.util.Annotations;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AnnotationExprFactory implements ElementFactory {

    private final Map<Strictness, MemberValuePair> mockitoSettingsMap;

    public AnnotationExprFactory(Map<Strictness, MemberValuePair> mockitoSettingsMap) {
        this.mockitoSettingsMap = mockitoSettingsMap;
    }

    public AnnotationExpr extendWith(ClassExpr extensionClass) {
        return new SingleMemberAnnotationExpr(
            StaticJavaParser.parseName(ExtendWith.class.getSimpleName()),
            extensionClass
        );
    }

    public AnnotationExpr mockitoSettings(Strictness strictness) {
        return new NormalAnnotationExpr(
            StaticJavaParser.parseName(MockitoSettings.class.getSimpleName()),
            new NodeList<>(mockitoSettingsMap.get(strictness))
        );
    }

    public AnnotationExpr disabled(AnnotationExpr ignored) {
        Expression value = Annotations.getValue(ignored);

        Name disabledName = StaticJavaParser.parseName(Disabled.class.getSimpleName());

        if (value == null) {
            return new MarkerAnnotationExpr(disabledName);
        } else {
            return new SingleMemberAnnotationExpr(disabledName, value);
        }
    }
}
