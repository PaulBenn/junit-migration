package uk.co.paulbenn.jupiterify.service.detector;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.AnnotationExpr;
import org.junit.Test;
import org.springframework.stereotype.Service;

@Service
public class JUnitDetector implements FrameworkDetector {

    @Override
    public Framework supportedFramework() {
        return Framework.JUNIT;
    }

    @Override
    public boolean test(CompilationUnit compilationUnit) {
        return compilationUnit.findFirst(
            AnnotationExpr.class,
            a -> Test.class.getSimpleName().equals(a.getNameAsString())
        ).isPresent();
    }
}
