package uk.co.paulbenn.jupiterify.service.visitor;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.ModifierVisitor;

import java.util.List;

public abstract class VisitorChain<A> {

    private final List<ModifierVisitor<A>> visitors;

    private final A argument;

    public VisitorChain(List<ModifierVisitor<A>> visitors, A argument) {
        this.visitors = visitors;
        this.argument = argument;
    }

    A getArgument() {
        return argument;
    }

    protected void afterEach(CompilationUnit compilationUnit) {

    }

    public void visit(CompilationUnit compilationUnit) {
        for (ModifierVisitor<A> visitor : visitors) {
            compilationUnit.accept(visitor, argument);
            afterEach(compilationUnit);
        }
    }
}
