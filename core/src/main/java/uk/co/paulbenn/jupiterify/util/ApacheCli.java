package uk.co.paulbenn.jupiterify.util;

import org.apache.commons.cli.PatternOptionBuilder;

import java.io.File;
import java.util.Date;

public class ApacheCli {

    /**
     * An array of Apache CLI's supported option types, as of version 1.4.
     *
     * The library throws {@code UnsupportedOperationException} on attempting to parse the following option types:
     * <ul>
     *     <li>{@link PatternOptionBuilder#DATE_VALUE} ({@link Date})</li>
     *     <li>{@link PatternOptionBuilder#FILES_VALUE} ({@link File[]})</li>
     * </ul>
     *
     * @see org.apache.commons.cli.TypeHandler
     */
    private static final Class<?>[] SUPPORTED_TYPES = new Class<?>[] {
        PatternOptionBuilder.STRING_VALUE,
        PatternOptionBuilder.OBJECT_VALUE,
        PatternOptionBuilder.NUMBER_VALUE,
        PatternOptionBuilder.CLASS_VALUE,
        PatternOptionBuilder.EXISTING_FILE_VALUE,
        PatternOptionBuilder.FILE_VALUE,
        PatternOptionBuilder.URL_VALUE,
    };

    public static Class<?>[] supportedTypes() {
        return SUPPORTED_TYPES;
    }

    public static boolean isSupported(Class<?> type) {
        for (Class<?> supportedType : supportedTypes()) {
            if (supportedType.equals(type)) {
                return true;
            }
        }
        return false;
    }
}
