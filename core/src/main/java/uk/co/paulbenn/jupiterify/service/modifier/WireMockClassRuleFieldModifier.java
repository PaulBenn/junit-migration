package uk.co.paulbenn.jupiterify.service.modifier;

import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import org.springframework.stereotype.Component;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;

@Component
public class WireMockClassRuleFieldModifier extends FieldModifier {

    @Override
    public boolean canModify(FieldDeclaration fieldDeclaration) {
        return fieldDeclaration.getVariables().stream().anyMatch(
            v -> WireMockClassRule.class.getSimpleName().equals(v.getType().asString()));
    }

    @Override
    public void modifyInPlace(FieldDeclaration fieldDeclaration) {
        if (StaticConfiguration.get().isAttemptWireMockMigration()) {
            fieldDeclaration.remove();
        }
    }
}
