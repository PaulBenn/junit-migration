package uk.co.paulbenn.jupiterify.service.factory;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.type.Type;
import org.junit.jupiter.api.extension.Extension;
import org.junit.runner.Runner;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ClassExprFactory implements ElementFactory {

    private final Map<Class<? extends Runner>, Class<? extends Extension>> runnerToExtension;

    public ClassExprFactory(Map<Class<? extends Runner>, Class<? extends Extension>> runnerToExtension) {
        this.runnerToExtension = runnerToExtension;
    }

    public ClassExpr extensionClass(ClassExpr runnerClass) {
        return extensionType(runnerClass.getType())
            .map(ClassExpr::new)
            .orElse(runnerClass);
    }

    private Optional<Type> extensionType(Type runnerType) {
        for (var entry : runnerToExtension.entrySet()) {
            if (runnerType.equals(parseType(entry.getKey()))) {
                return Optional.of(parseType(entry.getValue()));
            }
        }

        return Optional.empty();
    }

    private Type parseType(Class<?> type) {
        String[] parts = type.getName().split("\\.");
        return StaticJavaParser.parseType(parts[parts.length - 1].replace("$", "."));
    }
}
