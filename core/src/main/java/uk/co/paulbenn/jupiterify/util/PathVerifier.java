package uk.co.paulbenn.jupiterify.util;

import java.nio.file.Files;
import java.nio.file.Path;

public class PathVerifier {
    public static void verifyExists(Path path) {
        if (!Files.exists(path)) {
            throw new IllegalArgumentException("Path '" + path + "' does not exist");
        }
    }

    public static void verifyIsReadable(Path path) {
        if (!Files.isReadable(path)) {
            throw new IllegalArgumentException("Path '" + path + "' exists, but is not readable");
        }
    }

    public static void verifyIsWritable(Path path) {
        if (!Files.isWritable(path)) {
            throw new IllegalArgumentException("Path '" + path + "' exists, but is not writable");
        }
    }

    public static void verifyIsRegularFile(Path path) {
        if (!Files.isRegularFile(path)) {
            throw new IllegalArgumentException("Path '" + path + "' is not a file");
        }
    }

    public static void verifyIsDirectory(Path path) {
        if (!Files.isDirectory(path)) {
            throw new IllegalArgumentException("Path '" + path + "' is not a directory");
        }
    }

    public static void verifyExtension(Path path, String extension) {
        extension = extension.startsWith(".") ? extension : ("." + extension);

        if (!path.toString().endsWith(extension)) {
            throw new IllegalArgumentException("Path '" + path + "' is not a '" + extension + "' file");
        }
    }
}
