package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import org.junit.AfterClass;
import org.junit.jupiter.api.AfterAll;
import org.springframework.stereotype.Component;

@Component
public class AfterClassMethodModifier extends MethodModifier {

    @Override
    public boolean canModify(MethodDeclaration methodDeclaration) {
        return methodDeclaration.getAnnotationByClass(AfterClass.class).isPresent();
    }

    @Override
    protected NodeList<Modifier> newModifiers(MethodDeclaration methodDeclaration) {
        return new NodeList<>(Modifier.staticModifier());
    }

    @Override
    public NodeList<AnnotationExpr> newAnnotations(MethodDeclaration methodDeclaration) {
        return new NodeList<>(
            new MarkerAnnotationExpr(AfterAll.class.getSimpleName())
        );
    }
}
