package uk.co.paulbenn.jupiterify.config;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import uk.co.paulbenn.jupiterify.util.ApacheCli;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.stream.Collectors;

public class CommandLineWrapper {

    private final CommandLine commandLine;

    public CommandLineWrapper(CommandLine commandLine) {
        this.commandLine = commandLine;
    }

    // types from PatternOptionBuilder

    public String getString(Option option) {
        return castValue(option, String.class);
    }

    public <T> T getObjectInstance(Option option, Class<T> type) {
        return castValue(option, type);
    }

    public Number getNumber(Option option) {
        return castValue(option, Number.class);
    }

    public Date getDate(Option option) {
        return getDate(option, new SimpleDateFormat("yyyy-MM-dd"));
    }

    public Date getDate(Option option, DateFormat format) {
        String date = castValue(option, String.class);
        try {
            return date == null ? null : format.parse(date);
        } catch (java.text.ParseException e) {
            throw new CliSyntaxException(option, "cannot parse date from input '" + date + "'", e);
        }
    }

    public Class<?> getClass(Option option) {
        return castValue(option, Class.class);
    }

    public File getFile(Option option) {
        return castValue(option, File.class);
    }

    public File[] getFiles(Option option) {
        String s = getString(option);
        return s == null ? new File[0] : Arrays.stream(s.split(",\\s*")).map(File::new).toArray(File[]::new);
    }

    public URL getURL(Option option) {
        return castValue(option, URL.class);
    }

    // other supported types

    public String[] getStrings(Option option) {
        String s = castValue(option, String.class);
        return s == null ? new String[0] : s.split(",\\s*");
    }

    public Path getPath(Option option) {
        String s = getString(option);
        return s == null ? null : Paths.get(s);
    }

    public Long getLong(Option option) {
        String s = getString(option);
        return s == null ? null : Long.parseLong(s);
    }

    public Integer getInt(Option option) {
        String s = getString(option);
        return s == null ? null : Integer.parseInt(getString(option));
    }

    public Short getShort(Option option) {
        String s = getString(option);
        return s == null ? null : Short.parseShort(s);
    }

    public Byte getByte(Option option) {
        String s = getString(option);
        return s == null ? null : Byte.parseByte(s);
    }

    public Character getChar(Option option) {
        String s = getString(option);

        if (s == null) {
            return null;
        }

        if (s.length() != 1) {
            throw new CliSyntaxException(option, "cannot parse char from string '" + s + "' of length != 1");
        }

        return s.charAt(0);
    }

    public Boolean getBoolean(Option option) {
        String value = getString(option);

        if (value != null) {
            return Boolean.parseBoolean(value);
        }

        String key = (option.getOpt() == null) ? option.getLongOpt() : option.getOpt();
        return commandLine.hasOption(key);
    }

    public String getCustomProperty(String key) {
        return getCustomProperty(key, "");
    }

    public String getCustomProperty(String key, String defaultValue) {
        return (String) getCustomProperties().getOrDefault(key, defaultValue);
    }

    private <T> T castValue(Option option, Class<T> type) {
        checkOptionTypeSupported(option);
        return type.cast(getValue(option));
    }

    private void checkOptionTypeSupported(Option option) {
        Class<?> type = (Class<?>) option.getType();
        if (!ApacheCli.isSupported(type)) {
            throw new CliSyntaxException(
                option,
                String.format(
                    "unsupported type '%s', must be one of [%s]. If in doubt, use %s.",
                    type.getCanonicalName(),
                    Arrays.stream(ApacheCli.supportedTypes())
                        .map(Class::getSimpleName)
                    .collect(Collectors.joining(", ")),
                    String.class.getCanonicalName()
                )
            );
        }
    }

    private Object getValue(Option option) {
        try {
            return commandLine.getParsedOptionValue(option.getOpt());
        } catch (ParseException e) {
            throw new CliSyntaxException(
                option,
                "value '" + commandLine.getOptionValue(option.getOpt()) + "' cannot be parsed as type " + option.getType(),
                e
            );
        }
    }

    private Properties getCustomProperties() {
        Option option = ConfigurationOptions.CUSTOM_PROPERTY;
        String opt = (option.getOpt() == null) ? option.getLongOpt() : option.getOpt();

        return commandLine.getOptionProperties(opt);
    }
}
