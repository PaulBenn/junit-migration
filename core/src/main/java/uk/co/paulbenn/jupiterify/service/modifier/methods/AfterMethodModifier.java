package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.springframework.stereotype.Component;

@Component
public class AfterMethodModifier extends MethodModifier {

    @Override
    public boolean canModify(MethodDeclaration methodDeclaration) {
        return methodDeclaration.getAnnotationByClass(After.class).isPresent();
    }

    @Override
    public void modifyInPlace(MethodDeclaration methodDeclaration) {
        methodDeclaration.setName(StaticJavaParser.parseSimpleName("tearDown"));
        super.modifyInPlace(methodDeclaration);
    }

    @Override
    public NodeList<AnnotationExpr> newAnnotations(MethodDeclaration methodDeclaration) {
        // any possible @Override stemming from an "extends TestCase" needs to go.
        return new NodeList<>(
            new MarkerAnnotationExpr(AfterEach.class.getSimpleName())
        );
    }
}
