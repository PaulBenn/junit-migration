package uk.co.paulbenn.jupiterify.service.modifier.methods;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.stereotype.Component;
import uk.co.paulbenn.jupiterify.service.CommentRegistry;
import uk.co.paulbenn.jupiterify.service.factory.ElementFactories;
import uk.co.paulbenn.jupiterify.unfixable.NonEmptyTestAnnotation;

@Component
public class TestMethodModifier extends MethodModifier {

    private final CommentRegistry commentRegistry;
    private final ElementFactories elementFactories;

    @Override
    public boolean canModify(MethodDeclaration methodDeclaration) {
        return methodDeclaration.getAnnotationByClass(Test.class).isPresent();
    }

    public TestMethodModifier(CommentRegistry commentRegistry,
                              ElementFactories elementFactories) {
        this.commentRegistry = commentRegistry;
        this.elementFactories = elementFactories;
    }

    @Override
    protected NodeList<AnnotationExpr> newAnnotations(MethodDeclaration methodDeclaration) {
        NodeList<AnnotationExpr> annotations = methodDeclaration.getAnnotations();
        NodeList<AnnotationExpr> replacements = new NodeList<>();

        for (AnnotationExpr annotation : annotations) {
            if (Test.class.getSimpleName().equals(annotation.getNameAsString())) {
                replacements.add(
                    new MarkerAnnotationExpr(org.junit.jupiter.api.Test.class.getSimpleName())
                );

                if (annotation.isNormalAnnotationExpr() && annotation.asNormalAnnotationExpr().getPairs().isNonEmpty()) {
                    NonEmptyTestAnnotation testAnnotation = new NonEmptyTestAnnotation(annotation.asNormalAnnotationExpr());
                    Expression expected = testAnnotation.getExpected();
                    if (expected.isClassExpr()) {
                        methodDeclaration.getThrownExceptions()
                            .removeIf(
                                referenceType -> referenceType.equals(expected.asClassExpr().getType())
                            );
                    }
                    commentRegistry.registerUnfixable(methodDeclaration, testAnnotation);
                }
            } else if (Ignore.class.getSimpleName().equals(annotation.getNameAsString())) {
                replacements.add(elementFactories.annotationExprFactory().disabled(annotation));
            } else {
                replacements.add(annotation);
            }
        }

        return replacements;
    }
}
