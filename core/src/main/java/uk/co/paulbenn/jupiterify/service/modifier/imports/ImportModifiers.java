package uk.co.paulbenn.jupiterify.service.modifier.imports;

import com.github.javaparser.ast.ImportDeclaration;
import uk.co.paulbenn.jupiterify.service.modifier.NoOpModifier;
import uk.co.paulbenn.jupiterify.service.modifier.NodeModifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImportModifiers {

    private final List<ImportModifier> importModifiers;

    public ImportModifiers(List<ImportModifier> importModifiers) {
        this.importModifiers = importModifiers;
    }

    public NodeModifier<ImportDeclaration> getModifier(ImportDeclaration importDeclaration) {
        for (ImportModifier importModifier : importModifiers) {
            if (importModifier.canModify(importDeclaration)) {
                return importModifier;
            }
        }

        return NoOpModifier.instance();
    }
}
