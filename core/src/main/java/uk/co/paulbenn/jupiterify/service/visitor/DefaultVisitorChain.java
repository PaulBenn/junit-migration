package uk.co.paulbenn.jupiterify.service.visitor;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import org.springframework.stereotype.Service;
import uk.co.paulbenn.jupiterify.service.CommentRegistry;
import uk.co.paulbenn.jupiterify.service.ObserverDeDuplicator;
import uk.co.paulbenn.jupiterify.util.Comments;
import uk.co.paulbenn.jupiterify.util.DataKeys;

import java.util.List;

@Service
public class DefaultVisitorChain extends VisitorChain<CommentRegistry> {

    private final ObserverDeDuplicator observerDeDuplicator;

    public DefaultVisitorChain(List<ModifierVisitor<CommentRegistry>> modifierVisitors,
                               CommentRegistry commentRegistry,
                               ObserverDeDuplicator observerDeDuplicator) {
        super(modifierVisitors, commentRegistry);
        this.observerDeDuplicator = observerDeDuplicator;
    }

    @Override
    protected void afterEach(CompilationUnit compilationUnit) {
        compilationUnit.walk(node -> {
            if (node.containsData(DataKeys.UUID())) {
                observerDeDuplicator.deDuplicateObservers(node);
                Comment comment = Comments.commentForNode(
                    node,
                    getArgument().getComment(node)
                );
                node.getComment().ifPresent(Comment::remove);
                node.setComment(comment);
            }
        });

        super.afterEach(compilationUnit);
    }
}
