package uk.co.paulbenn.jupiterify;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import uk.co.paulbenn.jupiterify.config.Configuration;
import uk.co.paulbenn.jupiterify.config.ConfigurationParser;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigrationService;
import uk.co.paulbenn.jupiterify.service.SourceCodeIO;
import uk.co.paulbenn.jupiterify.util.PathVerifier;
import uk.co.paulbenn.jupiterify.util.SourceCodePaths;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Component
@Profile("!test")
@RequiredArgsConstructor
public class ApplicationCli implements CommandLineRunner {

    private final ConfigurationParser configurationParser;
    private final JupiterMigrationService jupiterMigrationService;

    @Override
    public void run(String... args) {
        Configuration configuration = configurationParser.fromCommandLine(args);
        StaticConfiguration.set(configuration);

        migrateFromCli();
    }

    public void migrateFromCli() {
        for (String sourceSet : StaticConfiguration.get().getSourceSets()) {
            Path pathToSourceSet = StaticConfiguration.get().getProjectRoot()
                .resolve(Paths.get("src", sourceSet, "java"));

            PathVerifier.verifyIsDirectory(pathToSourceSet);

            walk(pathToSourceSet)
                .filter(Files::isRegularFile)
                .filter(p -> p.toString().endsWith(".java"))
                .forEach(
                    p -> {
                        JupiterMigration jupiterMigration = new JupiterMigration(p, SourceCodeIO.readSource(p));
                        String jupiterJava = jupiterMigrationService.migrate(jupiterMigration);
                        SourceCodeIO.writeSource(SourceCodePaths.deriveOutputPath(p), jupiterJava);
                    }
                );
        }
    }

    private Stream<Path> walk(Path start) {
        try {
            return Files.walk(start);
        } catch (IOException e) {
            throw new UncheckedIOException("cannot start filesystem tree traversal from path " + start, e);
        }
    }
}
