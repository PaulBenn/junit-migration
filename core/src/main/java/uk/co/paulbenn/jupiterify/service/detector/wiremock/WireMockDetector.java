package uk.co.paulbenn.jupiterify.service.detector.wiremock;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import uk.co.paulbenn.jupiterify.service.detector.Framework;
import uk.co.paulbenn.jupiterify.service.detector.FrameworkDetector;
import org.springframework.stereotype.Service;

@Service
public class WireMockDetector implements FrameworkDetector {

    @Override
    public Framework supportedFramework() {
        return Framework.WIREMOCK;
    }

    @Override
    public boolean test(CompilationUnit compilationUnit) {
        return compilationUnit.findFirst(
            FieldDeclaration.class,
            f -> f.getVariables().stream()
                .anyMatch(
                    v -> WireMockClassRule.class.getSimpleName()
                        .equals(v.getType().asString())
                )
        ).isPresent();
    }
}
