package uk.co.paulbenn.jupiterify.service.modifier.imports;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.Name;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentMatchers;
import org.mockito.Matchers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StaticImportConfiguration {

    @Bean
    public StaticImportModifier assertions() {
        return new StaticImportModifier(Assert.class, Assertions.class) {
            @Override
            public void modifyInPlace(ImportDeclaration importDeclaration) {
                if (importDeclaration.getNameAsString().endsWith("assertThat")) {
                    // Hamcrest matchers
                    Name replacementName = StaticJavaParser.parseName(
                        org.hamcrest.MatcherAssert.class.getCanonicalName() + ".assertThat"
                    );
                    importDeclaration.getName().replace(replacementName);
                } else {
                    super.modifyInPlace(importDeclaration);
                }
            }
        };
    }

    @Bean
    public StaticImportModifier matchers() {
        return new StaticImportModifier(Matchers.class, ArgumentMatchers.class);
    }
}
