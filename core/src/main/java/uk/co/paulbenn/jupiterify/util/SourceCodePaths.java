package uk.co.paulbenn.jupiterify.util;

import uk.co.paulbenn.jupiterify.config.StaticConfiguration;

import java.nio.file.Path;

public class SourceCodePaths {

    public static Path deriveOutputPath(Path inputPath) {
        return StaticConfiguration.get().getOutputDirectory()
            .resolve(
                StaticConfiguration.get().getProjectRoot().relativize(inputPath)
            );
    }

    public static Path deriveInputPath(Path inputPath) {
        return StaticConfiguration.get().getProjectRoot()
            .resolve(
                StaticConfiguration.get().getOutputDirectory().relativize(inputPath)
            );
    }
}
