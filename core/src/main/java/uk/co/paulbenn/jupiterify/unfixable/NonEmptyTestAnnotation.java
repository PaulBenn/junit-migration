package uk.co.paulbenn.jupiterify.unfixable;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;

public class NonEmptyTestAnnotation implements Unfixable {
    private Expression expected = null;
    private long timeout = -1;

    public NonEmptyTestAnnotation(NormalAnnotationExpr normalAnnotationExpr) {
        NodeList<MemberValuePair> pairs = normalAnnotationExpr.getPairs();
        for (MemberValuePair pair : pairs) {
            if ("expected".equals(pair.getNameAsString())) {
                this.expected = pair.getValue();
            }
            if ("timeout".equals(pair.getNameAsString())) {
                // Prevent long overflow by parsing string contents
                this.timeout = Long.parseLong(pair.getValue().toString());
            }
        }
    }

    public Expression getExpected() {
        return expected;
    }

    @Override
    public String toDoStatement() {
        StringBuilder toDoStatement = new StringBuilder();
        toDoStatement
            .append("TODO")
            .append(System.lineSeparator())
            .append("The JUnit 4 @Test annotation contains key-value pairs that must be migrated manually:")
            .append(System.lineSeparator())
            .append(System.lineSeparator());

        if (expected != null) {
            toDoStatement
                .append("Key-value pair: (expected = ")
                .append(expected)
                .append(")")
                .append(System.lineSeparator())
                .append("Replacement:    assertThrows(")
                .append(expected)
                .append(", () -> code);")
                .append(System.lineSeparator());
        }

        if (timeout > 0) {
            toDoStatement
                .append("Key-value pair: (timeout = ")
                .append(timeout)
                .append(")")
                .append(System.lineSeparator())
                .append("Replacement:    assertTimeout(Duration.ofMillis(")
                .append(timeout)
                .append("), () -> code);")
                .append(System.lineSeparator());
        }

        return toDoStatement.toString();
    }
}
