package uk.co.paulbenn.jupiterify.util;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.ImmutableClassToInstanceMap;

import java.util.List;

public class ClassToInstanceMaps {

    public static <B, T extends B> ClassToInstanceMap<B> fromList(List<T> elements) {
        ImmutableClassToInstanceMap.Builder<B> builder = ImmutableClassToInstanceMap.builder();

        for (T element : elements) {
            Class<?> wildcardType = element.getClass();

            @SuppressWarnings("unchecked")
            Class<T> genericType = (Class<T>) wildcardType;

            builder.put(genericType, element);
        }

        return builder.build();
    }
}
