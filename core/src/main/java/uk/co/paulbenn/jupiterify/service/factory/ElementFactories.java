package uk.co.paulbenn.jupiterify.service.factory;

import com.google.common.collect.ClassToInstanceMap;
import uk.co.paulbenn.jupiterify.util.ClassToInstanceMaps;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ElementFactories {

    private final ClassToInstanceMap<ElementFactory> elementFactories;

    public ElementFactories(List<ElementFactory> elementFactories) {
        this.elementFactories = ClassToInstanceMaps.fromList(elementFactories);
    }

    public AnnotationExprFactory annotationExprFactory() {
        return elementFactories.getInstance(AnnotationExprFactory.class);
    }

    public ClassExprFactory classExprFactory() {
        return elementFactories.getInstance(ClassExprFactory.class);
    }
}
