package uk.co.paulbenn.jupiterify.config;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CommandLineWrapperTest {

    private CommandLineParser commandLineParser;

    @BeforeEach
    void setUp() {
        commandLineParser = new DefaultParser();
    }

    @Test
    void stringOption() {
        Option option = Option.builder("s").type(String.class).hasArg().build();

        CommandLineWrapper wrapper = parse(options(option), new String[]{ "-s", "abc" });

        assertEquals("abc", wrapper.getString(option));
    }

    @Test
    void objectInstanceOption() {
        Option option = Option.builder("o").hasArg().type(Object.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[]{ "-o", String.class.getCanonicalName() });

        assertEquals("", wrapper.getObjectInstance(option, String.class));
    }

    @Test
    void numberOption() {
        Option option = Option.builder("n").hasArg().type(Number.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[]{ "-n", "123" });

        assertEquals(123, wrapper.getNumber(option).intValue());
        assertEquals(123L, wrapper.getNumber(option).longValue());
    }

    @Test
    void dateOption() {
        Option option = Option.builder("d").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-d", "2020-01-01" });

        Calendar instance = Calendar.getInstance();
        instance.set(2020, Calendar.JANUARY, 1);
        assertThat(instance.getTime()).isEqualToIgnoringHours(wrapper.getDate(option));
    }

    @Test
    void dateOptionWithFormatter() {
        Option option = Option.builder("d").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-d", "01-01-2020" });

        Calendar instance = Calendar.getInstance();
        instance.set(2020, Calendar.JANUARY, 1);
        assertThat(instance.getTime()).isEqualToIgnoringHours(
            wrapper.getDate(option, new SimpleDateFormat("dd-MM-yyyy"))
        );
    }

    @Test
    void classOption() {
        Option option = Option.builder("c").hasArg().type(Class.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-c", String.class.getCanonicalName() });

        assertEquals(String.class, wrapper.getClass(option));
    }

    @Test
    void fileOption() {
        Option option = Option.builder("f").hasArg().type(File.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-f", "/path/to/file" });

        assertNotNull(wrapper.getFile(option));
    }

    @Test
    void filesOption() {
        Option option = Option.builder("fs").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-fs", "/path/to/file,/path/to/file" });

        assertThat(wrapper.getFiles(option)).hasSize(2);
        assertNotNull(wrapper.getFiles(option)[0]);
        assertNotNull(wrapper.getFiles(option)[1]);
    }

    @Test
    void urlOption() {
        Option option = Option.builder("u").hasArg().type(URL.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-u", "file:/path/to/file" });

        URL expected = assertDoesNotThrow(() -> new URL("file:/path/to/file"));
        assertEquals(expected, wrapper.getURL(option));
    }

    @Test
    void stringsOption() {
        Option option = Option.builder("ss").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-ss", "abc,def" });

        assertArrayEquals(new String[]{ "abc", "def" }, wrapper.getStrings(option));
    }

    @Test
    void pathOption() {
        Option option = Option.builder("p").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-p", "/path/to/file" });

        assertEquals(Paths.get("/path/to/file"), wrapper.getPath(option));
    }

    @Test
    void longOption() {
        Option option = Option.builder("l").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-l", "123" });

        assertEquals(123L, wrapper.getLong(option));
    }

    @Test
    void intOption() {
        Option option = Option.builder("l").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-l", "123" });

        assertEquals(123, wrapper.getInt(option));
    }

    @Test
    void shortOption() {
        Option option = Option.builder("s").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-s", "123" });

        assertEquals((short) 123, wrapper.getShort(option));
    }

    @Test
    void byteOption() {
        Option option = Option.builder("b").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-b", "123" });

        assertEquals((byte) 123, wrapper.getShort(option));
    }

    @Test
    void charOption() {
        Option option = Option.builder("c").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-c", "c" });

        assertEquals('c', wrapper.getChar(option));
    }

    @Test
    void charOptionTooLong() {
        Option option = Option.builder("c").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-c", "ccc" });

        assertThrows(CliSyntaxException.class, () -> wrapper.getChar(option));
    }

    @Test
    void booleanOptionWithArgTrue() {
        Option option = Option.builder("b").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-b", "true" });

        assertTrue(wrapper.getBoolean(option));
    }

    @Test
    void booleanOptionWithArgFalse() {
        Option option = Option.builder("b").hasArg().type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-b", "false" });

        assertFalse(wrapper.getBoolean(option));
    }

    @Test
    void booleanOptionWithoutArgTrue() {
        Option option = Option.builder("b").type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-b" });

        assertTrue(wrapper.getBoolean(option));
    }

    @Test
    void booleanOptionWithoutArgFalse() {
        Option option = Option.builder("b").type(String.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { });

        assertFalse(wrapper.getBoolean(option));
    }

    @Test
    void customOption() {
        Option option = ConfigurationOptions.CUSTOM_PROPERTY;

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-X", "key=value" });

        assertEquals("value", wrapper.getCustomProperty("key"));
    }

    @Test
    void customOptionWithNoKey() {
        Option option = ConfigurationOptions.CUSTOM_PROPERTY;

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-X", "=value" });

        assertEquals("", wrapper.getCustomProperty("any"));
    }

    @Test
    void customOptionWithNoValue() {
        Option option = ConfigurationOptions.CUSTOM_PROPERTY;

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-X", "key=" });

        assertEquals("", wrapper.getCustomProperty("key"));
    }

    @Test
    void customOptionWithNoKeyOrValue() {
        Option option = ConfigurationOptions.CUSTOM_PROPERTY;

        CommandLineWrapper wrapper = parse(options(option), new String[] { "-X", "=" });

        assertEquals("", wrapper.getCustomProperty("any"));
    }

    @Test
    void unsupportedOptionType() {
        Option option = Option.builder("b").type(Date.class).build();

        CommandLineWrapper wrapper = parse(options(option), new String[] { });

        assertThrows(CliSyntaxException.class, () -> wrapper.getDate(option));
    }

    private Options options(Option... options) {
        Options all = new Options();
        for (Option option : options) {
            all.addOption(option);
        }
        return all;
    }

    private CommandLineWrapper parse(Options options, String[] args) {
        CommandLine commandLine = assertDoesNotThrow(() -> commandLineParser.parse(options, args));
        return new CommandLineWrapper(commandLine);
    }
}
