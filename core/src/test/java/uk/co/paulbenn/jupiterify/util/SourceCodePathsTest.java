package uk.co.paulbenn.jupiterify.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import uk.co.paulbenn.jupiterify.config.Configuration;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SourceCodePathsTest {

    @AfterEach
    void tearDown() {
        StaticConfiguration.clear();
    }

    @Test
    void noAbsolutePaths() {
        Configuration configuration = new Configuration();

        configuration.setProjectRoot(Paths.get("project/root"));
        configuration.setOutputDirectory(Paths.get("output/dir"));

        StaticConfiguration.set(configuration);

        assertEquals(
            Paths.get("output/dir/src/main/java/MyClass.java"),
            SourceCodePaths.deriveOutputPath(Paths.get("project/root/src/main/java/MyClass.java"))
        );
    }

    @Test
    void absoluteProjectRootPath() {
        Configuration configuration = new Configuration();

        configuration.setProjectRoot(Paths.get("/project/root"));
        configuration.setOutputDirectory(Paths.get("output/dir"));

        StaticConfiguration.set(configuration);

        assertEquals(
            Paths.get("output/dir/src/main/java/MyClass.java"),
            SourceCodePaths.deriveOutputPath(Paths.get("/project/root/src/main/java/MyClass.java"))
        );
    }

    @Test
    void absoluteOutputDirectoryPath() {
        Configuration configuration = new Configuration();

        configuration.setProjectRoot(Paths.get("project/root"));
        configuration.setOutputDirectory(Paths.get("/output/dir"));

        StaticConfiguration.set(configuration);

        assertEquals(
            Paths.get("/output/dir/src/main/java/MyClass.java"),
            SourceCodePaths.deriveOutputPath(Paths.get("project/root/src/main/java/MyClass.java"))
        );
    }

    @Test
    void noRelativePaths() {
        Configuration configuration = new Configuration();

        configuration.setProjectRoot(Paths.get("/project/root"));
        configuration.setOutputDirectory(Paths.get("/output/dir"));

        StaticConfiguration.set(configuration);

        assertEquals(
            Paths.get("/output/dir/src/main/java/MyClass.java"),
            SourceCodePaths.deriveOutputPath(Paths.get("/project/root/src/main/java/MyClass.java"))
        );
    }
}
