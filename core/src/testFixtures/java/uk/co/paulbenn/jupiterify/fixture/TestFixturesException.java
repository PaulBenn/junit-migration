package uk.co.paulbenn.jupiterify.fixture;

public class TestFixturesException extends RuntimeException {

    public TestFixturesException(String message) {
        super("testFixtures: " + message);
    }

    public TestFixturesException(String message, Throwable cause) {
        super("testFixtures: " + message, cause);
    }
}
