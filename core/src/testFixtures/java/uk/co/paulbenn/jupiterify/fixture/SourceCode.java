package uk.co.paulbenn.jupiterify.fixture;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Arrays;
import java.util.Optional;

public class SourceCode {

    public static Path getSourceCodePath(Class<?> clazz) {
        if (clazz.isMemberClass()) {
            throw new TestFixturesException("class '" + clazz + "' is nested. only top-level classes supported");
        }

        return pathToDotJava(clazz);
    }

    private static Path pathToDotJava(Class<?> clazz) {
        return pathToProjectRoot(clazz)
            .resolve(Paths.get("src", "main", "java"))
            .resolve(pathWithinPackage(clazz) + ".java");
    }

    private static Path pathToProjectRoot(Class<?> clazz) {
        Path dotClassLocation = pathToDotClass(clazz);

        Path current = dotClassLocation;

        while(!current.endsWith("classes")) {
            Path parent = current.getParent();

            if (parent == null) {
                throw new TestFixturesException("reached filesystem root without encountering '/classes' directory");
            }

            current = parent;
        }

        // "current" at this point is the absolute path to the build folder's /classes directory.

        Path buildFolder = current.getParent();

        String[] supportedBuildFolders = { "build", "target" };

        for (String supportedBuildFolder : supportedBuildFolders) {
            if (buildFolder.endsWith(supportedBuildFolder)) {
                // The project root is two directories up from <build-folder>/classes.
                return current.getParent().getParent();
            }
        }

        throw new TestFixturesException(
            String.format(
                "unsupported .class location '%s': build folder must be one of %s",
                dotClassLocation,
                Arrays.toString(supportedBuildFolders)
            )
        );
    }

    private static Path pathToDotClass(Class<?> clazz) {
        return Paths.get(getUri(clazz)).resolve(pathWithinPackage(clazz) + ".class");
    }

    private static URI getUri(Class<?> clazz) {
        URL classUrl = Optional.of(clazz.getProtectionDomain())
            .map(ProtectionDomain::getCodeSource)
            .map(CodeSource::getLocation)
            .orElseThrow(
                () -> new TestFixturesException(
                    "null source location for class '" + clazz.getCanonicalName() + "'"
                )
            );

        try {
            return classUrl.toURI();
        } catch (URISyntaxException e) {
            throw new TestFixturesException("malformed URL '" + classUrl + "' for class '" + clazz + "'", e);
        }
    }

    private static Path pathWithinPackage(Class<?> clazz) {
        return Paths.get(clazz.getCanonicalName().replace('.', File.separatorChar));
    }
}
