package uk.co.paulbenn.jupiterify.assertions;

import uk.co.paulbenn.jupiterify.service.migration.JupiterMigration;
import uk.co.paulbenn.jupiterify.fixture.SourceCode;
import uk.co.paulbenn.jupiterify.fixture.TestFixturesException;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigrationService;
import uk.co.paulbenn.jupiterify.service.SourceCodeIO;
import uk.co.paulbenn.jupiterify.util.PathVerifier;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AssertMigrated {

    public static void assertMigratedCorrectly(Class<?> jUnit4, JupiterMigrationService jupiterMigrationService) {
        Class<?> expectedJUnit5 = expectedJUnit5Class(jUnit4);
        Path pathToJunit4Java = getVerifiedPath(jUnit4);
        Path pathToJupiterJava = getVerifiedPath(expectedJUnit5);

        // Compare as strings to make sure lexical preservation is working as intended
        String expectedJupiterJava = SourceCodeIO.readSource(pathToJupiterJava);
        String actualJupiterJava = jupiterMigrationService.migrate(
            new JupiterMigration(pathToJunit4Java, SourceCodeIO.readSource(pathToJunit4Java))
        );

        // Match package declaration to original, as Jupiterify will not change this.
        expectedJupiterJava = expectedJupiterJava.replace(
            "package " + expectedJUnit5.getPackageName() + ";",
            "package " + jUnit4.getPackageName() + ";"
        );

        // Ignore import order (force them to alphabetical order before comparison)
        expectedJupiterJava = alphabeticallyOrderImports(expectedJupiterJava);
        actualJupiterJava = alphabeticallyOrderImports(actualJupiterJava);

        assertEquals(expectedJupiterJava, actualJupiterJava);
    }

    private static Class<?> expectedJUnit5Class(Class<?> jUnit4Class) {
        try {
            return Class.forName(jUnit4Class.getCanonicalName().replace(".junit4.", ".junit5."));
        } catch (ClassNotFoundException e) {
            throw new TestFixturesException(
                String.format(
                    "JUnit 4 class '%s' has no JUnit 5 equivalent",
                    jUnit4Class.getCanonicalName()
                ),
                e
            );
        }
    }

    private static String alphabeticallyOrderImports(String sourceCode) {
        String[] lines = sourceCode.split("\\R");

        List<Integer> importLineIndexes = new ArrayList<>();
        List<String> importLines = new ArrayList<>();

        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (line.startsWith("import ") && line.endsWith(";")) {
                importLineIndexes.add(i);
                importLines.add(line);
            }
        }

        importLineIndexes.sort(Comparator.naturalOrder());
        importLines.sort(Comparator.naturalOrder());

        int sortedImportLinesIndex = 0;
        for(int index : importLineIndexes) {
            lines[index] = importLines.get(sortedImportLinesIndex++);
        }

        return String.join(System.lineSeparator(), lines);
    }

    private static Path getVerifiedPath(Class<?> clazz) {
        Path pathToClass = SourceCode.getSourceCodePath(clazz);

        PathVerifier.verifyExists(pathToClass);
        PathVerifier.verifyIsReadable(pathToClass);
        PathVerifier.verifyIsRegularFile(pathToClass);
        PathVerifier.verifyExtension(pathToClass, ".java");

        return pathToClass;
    }
}
