package uk.co.paulbenn.jupiterify;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import uk.co.paulbenn.jupiterify.assertions.AssertMigrated;
import uk.co.paulbenn.jupiterify.config.Configuration;
import uk.co.paulbenn.jupiterify.config.StaticConfiguration;
import uk.co.paulbenn.jupiterify.junit4.AllAnnotations;
import uk.co.paulbenn.jupiterify.junit4.Comments;
import uk.co.paulbenn.jupiterify.service.migration.JupiterMigrationService;

@SpringBootTest
@ActiveProfiles("test")
class FunctionalTest {

    @Autowired
    private JupiterMigrationService jupiterMigrationService;

    @BeforeAll
    static void setUp() {
        Configuration configuration = new Configuration();
        configuration.setDryRun(true);
        StaticConfiguration.set(configuration);
    }

    @Test
    void comments() {
        Class<?> jUnit4 = Comments.class;

        AssertMigrated.assertMigratedCorrectly(jUnit4, jupiterMigrationService);
    }

    @Test
    void allAnnotations() {
        Class<?> jUnit4 = AllAnnotations.class;

        AssertMigrated.assertMigratedCorrectly(jUnit4, jupiterMigrationService);
    }
}
