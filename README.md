# Jupiterify (formally AbsoluteJUnit)
Migrate an entire project from JUnit 4 to JUnit 5 in one click.

## Requirements
* Existing code should compile. The tool is likely to ignore unparseable code.
* Dependencies for `junit-jupiter`, `mockito-core` and `mockito-junit-jupiter` must be manually added to the project's dependencies:

```xml
<!-- JUnit 5 -->
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter</artifactId>
    <version>${junit-jupiter.version}</version>
</dependency>
<!-- Mockito Core -->
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>${mockito-core.version}</version>
</dependency>
<!-- Mockito JUnit 5 extension -->
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-junit-jupiter</artifactId>
    <version>${mockito-junit-jupiter.version}</version>
</dependency>
``` 

## Limitations
This project is based on JavaParser and shares some its limitations.
* Absolute JUnit **DOES NOT CONTAIN A SYMBOL SOLVER**. This means it is entirely possible that generated code does not compile under certain circumstances. You are responsible for inspecting the changes and seeing if you're happy with them.
* When you commit, ensure you tick `Optimize Imports` in IntelliJ's "Commit" dialog. The pretty-printer may display imports out of order, or may not separate them with spaces as an IDE would.
* Lexical preservation of code is achieved via an observer-based difference calculator within JavaParser that stores changes on the AST itself. This code is in the alpha stages and in rare cases, it's possible that the migration tool generates incorrectly indented code when two or more consecutive sibling nodes are changed (e.g. a comment and an annotation on the same method).
* Code in this project is not fully tested, documented or indeed clear (yet). The only guarantee I make is that using it will be faster than doing the migration manually.

## Features
* Migrates all `@Test`, `@Before`, `@After`, `@BeforeClass`, `@AfterClass` and `@Ignore` annotations to their JUnit 5 equivalents
* Migrates all `Matchers` usages to `ArgumentMatchers` - including adding the Hamcrest `assertThat` if necessary
* Migrates all replaceable `@RunWith` instances to `@ExtendWith` (with equivalent `Extension`)
* Makes test classes and applicable methods within them package-private
* If `-w` is specified, removes `WireMockClassRule` fields and adds a WireMock `Extension`
* If `-DuseCamelCase` is specified, migrates all test method names to the Java naming convention.
* Adds `TODO` comments to unfixable situations (such as `@Test(expected = Throwable.class)`) with a suggested replacement
* **Takes less than 20 seconds to migrate over 180 files.**
## Usage
1. Clone this project.
2. Run it via the command line or from an IntelliJ "Run Configuration".
3. Set the only mandatory program argument `-i` or `--input-dir` to the source root of the project you want to migrate.
4. Review changes.

### Command Line Options
TODO take this with a grain of salt - `-w` and `-X` are unstable

| Option           | Long Option                | Required | Default       | Description                               |
|------------------|----------------------------|----------|---------------|-------------------------------------------|
| `-p <directory>` | `--input-dir <directory>`  | Yes      | N/A           | Input directory (project root)            |
| `-o <directory>` | `--output-dir <directory>` | No       | `--input-dir` | Output directory (if different)           |
| `-w <f-q-class>` | `--wiremock <f-q-class>`   | No       | `null`        | WireMock `Extension` canonical class name |
| `-Xindent=2`     | N/A                        | No       | `4`           | Set minimum indent to 2 (**alpha**)       |
| `-XuseCamelCase` | N/A                        | No       | `false`       | migrate test method names to `camelCase`  |

## Examples

### Basic example
`java AbsoluteJUnit -p "C:\Development\repos\<your-junit4-project>"`

### Different output directory
```
java AbsoluteJUnit
    -p "C:\Development\repos\<your-junit4-project>"
    -o "C:\Development\repos\<your-junit4-project>\output"`
```

### WireMock migration
Specifying the `-w` argument will make Absolute Junit attempt to remove simple references to JUnit-4-compatible WireMock and add a JUnit 5 `Extension` for it. However, since there is no standard WireMock `Extension`, you must provide your own, such as:
* [The THG WireMock JUnit 5 extension](https://gitlab.io.thehut.local/performance-marketing/thg-wiremock-junit5)
* [lanwen's extension](https://github.com/lanwen/wiremock-junit5)
* [JensPiegsa's extension](https://github.com/JensPiegsa/wiremock-extension)

You must pass the `Extension` class as a canonical (fully-qualified) name.
```java AbsoluteJUnit
    -p "C:\Development\repos\<your-junit4-project>"
    -w com.thehutgroup.thgwiremockjunit5.WireMockHttpExtension
```

### Method names to camelCase
The Java naming convention for methods is `camelCase` as opposed to `snake_case`.
If you have thousands of legacy test methods named in `snake_case` it can be a pain to change them all manually.

Specifying `useCamelCase` will change all test method names. No other methods will be affected. Non-static set-up and tear-down methods will be named `setUp` and `tearDown` respectively regardless of this option, as per JUnit conventions.
```
java AbsoluteJUnit
    -p "C:\Development\repos\<your-junit4-project>"
    -DuseCamelCase
```

## Contributing
Some of the code in this project is hacky at best. Any help is welcome, as I do this in my own time. Some issues which need to be fixed:
* There are no tests.
* Comments added to a node occasionally require the node's observers to be deduplicated.
* The only way to modify `NodeList<N>` without causing a CME seems to be removing all children, then re-adding the desired children. There must be a better way to do this.
* With some careful reflection, it should be possible to make `LexicalPreservingPrinter` instantiable (and thus thread-safe).
* A better design would condense all `NodeModifier<N>` implementations into one injectable class and use it in the visitor chain.

Please submit a merge request if you're interested in fixing any of these or the countless problems I'm sure to have missed. Also feel free to message me directly on Teams if there's a simple improvement you'd like to see.

Thanks to Sam Haines for the excellent name Absolute JUnit!