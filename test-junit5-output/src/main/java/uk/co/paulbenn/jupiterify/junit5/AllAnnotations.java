package uk.co.paulbenn.jupiterify.junit5;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AllAnnotations {

    @BeforeAll
    static void setUpClass() {

    }

    @AfterAll
    static void tearDownClass() {

    }

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void aTestMethod() {

    }
}
