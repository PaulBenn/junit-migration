package uk.co.paulbenn.jupiterify.junit5;

import org.junit.jupiter.api.Test;

class Comments {

    // a comment before annotation
    @Test
    void commentBeforeAnnotation() {

    }

    @Test // a comment after annotation
    void commentAfterAnnotation() {

    }

    @Test
    // a comment on a new line after annotation
    void commentOnNewLineAfterAnnotation() {

    }

    @Test
    void commentInsideMethod() {
        // a comment inside method
    }

    @Test
    void commentInsideMethodWithLogicBeforeComment() {
        new Object();
        // a comment inside method
    }

    @Test
    void commentInsideMethodWithLogicAfterComment() {
        // a comment inside method
        new Object();
    }
}
