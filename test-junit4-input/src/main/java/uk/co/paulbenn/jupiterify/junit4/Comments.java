package uk.co.paulbenn.jupiterify.junit4;

import org.junit.Test;

public class Comments {

    // a comment before annotation
    @Test
    public void commentBeforeAnnotation() {

    }

    @Test // a comment after annotation
    public void commentAfterAnnotation() {

    }

    @Test
    // a comment on a new line after annotation
    public void commentOnNewLineAfterAnnotation() {

    }

    @Test
    public void commentInsideMethod() {
        // a comment inside method
    }

    @Test
    public void commentInsideMethodWithLogicBeforeComment() {
        new Object();
        // a comment inside method
    }

    @Test
    public void commentInsideMethodWithLogicAfterComment() {
        // a comment inside method
        new Object();
    }
}
